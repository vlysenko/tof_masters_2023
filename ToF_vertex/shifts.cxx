#include <TRandom3.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TFormula.h>
#include <TF1.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TColor.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TLatex.h>
#include <TMath.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

int main(int argc, char **argv)
{

    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }

    // setup
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);
    TFile *inf = new TFile(argv[1], "READ");

    int runNumber=429142;
    //int runNumber=435229;

    auto hists_tof_minus_bs = std::vector<std::vector<TH1D *>>(16, std::vector<TH1D *>(16));
    auto shifts_tof = std::vector<std::vector<float>>(16, std::vector<float>(16));
    auto stations = std::vector<std::string>{"A", "C"};
    auto channels = std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

    for (size_t i = 0; i < 16; i++)
        for (size_t j = 0; j < 16; j++)
            hists_tof_minus_bs[i][j] = (TH1D*)inf->Get(("tof_bs" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str());

    auto fitter_simple = [&](TH1D *hist)
    {
        if (hist->GetEntries() > 0)
        {
            TF1* gaus1 = new TF1("gaus1", "gaus");
            gaus1->SetLineColor(2);    
            TFitResultPtr r = hist->Fit("gaus1","S");
            double mean = r->Parameters()[1];
            delete gaus1;

            return mean;
        }
        else
        {return 0.0;}   
    };

    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            shifts_tof[i][j]=fitter_simple(hists_tof_minus_bs[i][j]);

    //shifts
    //TH2D *h2_shifts = new TH2D("h2_shifts", "h2_shifts", 16, -2720, -2520, 16, -2720, -2520);
    TH2D *h2_shifts = new TH2D("h2_shifts", "h2_shifts", 16, 0, 16, 16, 0, 16);
    TH1D *h1_shifts = new TH1D("h1_shifts", "h1_shifts", 256, 0, 260);
    
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
        {
            h2_shifts->SetBinContent(i+1, j+1, shifts_tof[i][j]);
            h1_shifts->SetBinContent((i*16+j+1), shifts_tof[i][j]);
        }

    h1_shifts->GetXaxis()->SetTitle("Index");
    h1_shifts->GetYaxis()->SetTitle("D_{C_{j}} - D_{A_{i}} [mm]");
    h2_shifts->GetXaxis()->SetTitle("Index D_{A_{i}}");
    h2_shifts->GetYaxis()->SetTitle("Index D_{C_{j}}");
    //h2_shifts->GetZaxis()->SetTitle("D_{C_{j}} - D_{A_{i}} [mm]");
    //h2_shifts->GetZaxis()->SetTitle("[mm]");

    TLatex latex;
    latex.SetNDC();
    latex.SetTextSize(0.04);        

    c1->SaveAs((std::string("shifts_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    h2_shifts->Draw("surf2");
    ATLASLabel_WithoutATLAS(0.78,0.97,"Internal");
    latex.DrawLatex(0.78,0.87,(std::string("Run ") + std::to_string(runNumber)).c_str()); 
    c1->SaveAs((std::string("shifts_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
    c1->Clear();
    h1_shifts->Draw();
    ATLASLabel_WithoutATLAS(0.65,0.95,"Internal");
    latex.DrawLatex(0.65,0.85,(std::string("Run ") + std::to_string(runNumber)).c_str()); 
    c1->SaveAs((std::string("shifts_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
    c1->Clear();
    c1->SaveAs((std::string("shifts_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());

}