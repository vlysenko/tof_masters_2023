#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <limits>

#include <TRandom3.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TFormula.h>
#include <TF1.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TColor.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TLatex.h>
#include <TMath.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

Double_t myFunc (Double_t *x, Double_t *pars) 
    {
        Float_t xx =x[0];
        Double_t f = 0;
        for (size_t i = 0; i < 16; i++)
            for (size_t j = 0; j < 16; j++)
                f = f + ( ((xx>i*16+j) && (xx<i*16+j+1)) * (pars[16+j]-pars[i]));      
        return f;
    }

int main(int argc, char **argv)
{

    if (argc != 4)
    {
        std::cout << "Requires 4 parameters\n";
        return 1;
    }

    // setup
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);
    TFile *inf_sh = new TFile(argv[1], "READ");

    auto in_files = std::vector<TFile *>(2);
    in_files[0]=new TFile(argv[2], "READ");
    in_files[1]=new TFile(argv[3], "READ");

    std::vector<float> *toFHit_channel = nullptr, *toFHit_trainID = nullptr, *toFHit_stationID = nullptr, 
                       *toFHit_time = nullptr, *toFHit_bar = nullptr, *pv_z = nullptr, *pv_vertexType = nullptr,
                       *aFPTrack_xLocal = nullptr, *aFPTrack_stationID = nullptr;

    int runNumber;
    int eventNumber;
    int lumi_block;
    float beam_pos;
    float speed_of_light = TMath::C();

    auto hists_tof_minus_bs = std::vector<std::vector<TH1D *>>(16, std::vector<TH1D *>(16));
    auto shifts_tof = std::vector<std::vector<float>>(16, std::vector<float>(16));
    auto stations = std::vector<std::string>{"A", "C"};
    auto channels = std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    auto params = std::vector<std::vector<float>>(2, std::vector<float>(16));

    for (size_t i = 0; i < 16; i++)
        for (size_t j = 0; j < 16; j++)
            hists_tof_minus_bs[i][j] = (TH1D*)inf_sh->Get(("tof_bs" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str());

    auto fitter_simple = [&](TH1D *hist)
    {
        if (hist->GetEntries() > 0)
        {
            TF1* gaus1 = new TF1("gaus1", "gaus");
            gaus1->SetLineColor(2);    
            TFitResultPtr r = hist->Fit("gaus1","S");
            double mean = r->Parameters()[1];
            delete gaus1;

            return mean;
        }
        else
        {return 0.0;}   
    };

    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            shifts_tof[i][j]=fitter_simple(hists_tof_minus_bs[i][j]);

    //parametrization from 256 pairs to 32 channels
    TH1D *h1_shifts = new TH1D("h1_shifts", "h1_shifts", 256, 0, 256);
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            h1_shifts->SetBinContent((i*16+j+1), shifts_tof[i][j]);

    //get 32 params
    TF1 * tf_parsmetrazation = new TF1("tf_parsmetrazation",myFunc, 0.1,255.9,32);
    for (int i = 0; i < 32; i++)
        tf_parsmetrazation->SetParameter(i, 1500);

    tf_parsmetrazation->SetNpx(1000);
    h1_shifts->Fit(tf_parsmetrazation,"R");

    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 16; j++)
            params[i][j] = tf_parsmetrazation->GetParameter(i*16+j);

    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 16; j++)
            params[i][j]=params[i][j]*2/(speed_of_light*pow(10,-6));


    auto n_bin = 150;
    TH1D *h_pv_atlas = new TH1D("pv_atlas", "pv_atlas", n_bin, -250, 250);
    TH1D *h_tof_vertex = new TH1D("vertex_tof", "vertex_tof", n_bin, -250, 250);
    TH1D *h_atlas_minus_tof = new TH1D("atlas_minus_tof", "atlas_minus_tof", n_bin*2, -250, 250);
    TH1D *h_atlas_minus_tof_close = new TH1D("atlas_minus_tof_close", "atlas_minus_tof_close", n_bin*2, -50, 50);
    
    float tof_vertex=0;
    float time=0;
    float tof_minus_bs;
    int chan;
    int i_A;
    int i_C;
    float ver_time_A=0;
    float ver_time_C=0;
    
    
    //main loop
    for (size_t files = 0; files<in_files.size(); files++)
    {
        TTree *tree_data = in_files[files]->Get<TTree>("CollectionTree");
        size_t nentries = tree_data->GetEntries();
        tree_data->SetBranchAddress("ToFHit_channel", &toFHit_channel);
        tree_data->SetBranchAddress("ToFHit_trainID", &toFHit_trainID);
        tree_data->SetBranchAddress("ToFHit_stationID", &toFHit_stationID);
        tree_data->SetBranchAddress("ToFHit_time", &toFHit_time);
        tree_data->SetBranchAddress("ToFHit_bar", &toFHit_bar);
        tree_data->SetBranchAddress("AFPTrack_xLocal", &aFPTrack_xLocal);
        tree_data->SetBranchAddress("AFPTrack_stationID", &aFPTrack_stationID);
        tree_data->SetBranchAddress("RunNumber", &runNumber);
        tree_data->SetBranchAddress("EventNumber", &eventNumber);
        tree_data->SetBranchAddress("lumiBlock", &lumi_block);
        tree_data->SetBranchAddress("beam_pos_z", &beam_pos);
        tree_data->SetBranchAddress("PV_vertexType", &pv_vertexType);
        tree_data->SetBranchAddress("PV_z", &pv_z);

        for (size_t i = 0; i < nentries; i++)
        //for (size_t i = 0; i < nentries/12; i++)
        {   
            if (runNumber == 429142)
                if (i % 300000 == 0)
                    std::cout << i/300000 << " from " << nentries/300000 << "\n";
            if (runNumber == 435229)
                if (i % 9000000 == 0)
                    std::cout << i/9000000 << " from " << nentries/9000000 << "\n";

            tree_data->GetEntry(i);

            if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
                continue;

            // cut in only 1 track per event
            int sum_tracks_A = 0, sum_tracks_C = 0;
            for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
            {
                if (aFPTrack_stationID->at(j) == 0)
                    sum_tracks_A++;
                if (aFPTrack_stationID->at(j) == 3)
                    sum_tracks_C++;
            }
            if (sum_tracks_A != 1 || sum_tracks_C != 1)
                continue;

            //cut on only 1 train per event
            std::vector<int> vec_train_A(4, 0), vec_train_C(4, 0);
            for (size_t j = 0; j < toFHit_trainID->size(); j++)
            {
                for (size_t h = 0; h < 4; h++)
                {
                    if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 0)
                        vec_train_A[h] = 1;
                    if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 3)
                        vec_train_C[h] = 1;
                }
            }
            int sum_train_A = 0, sum_train_C = 0;
            for (size_t h = 0; h < 4; h++)
            {
                sum_train_A += vec_train_A[h];
                sum_train_C += vec_train_C[h];
            }
            if ( (sum_train_A != 1) || (sum_train_C != 1) ) 
                continue;

            
            //cut on train2
            int train_out_A=0, train_out_C=0;
            for (size_t j = 0; j < toFHit_trainID->size(); j++)
            {
                if (toFHit_trainID->at(j) == 2 && toFHit_stationID->at(j) == 0)
                    train_out_A++;
                if (toFHit_trainID->at(j) == 2 && toFHit_stationID->at(j) == 3)
                    train_out_C++;
            }
            if (train_out_A>0 || train_out_C>0)
                continue;
            
           
            //cut on arrival time
            int count_times=0;
            for (size_t count = 0; count < toFHit_channel->size(); ++count)
            {
                time=0;
                if (toFHit_channel->at(count)>=16)
                    continue;
                if (toFHit_stationID->at(count) == 0)
                {
                    time = toFHit_time->at(count);
                    if (1024 / 25 * time > 1000 || 1024 / 25 * time < 900) 
                        count_times++;
                }
                if (toFHit_stationID->at(count) == 3)
                {
                    time = toFHit_time->at(count);
                    if (1024 / 25 * time > 300 || 1024 / 25 * time < 200) 
                        count_times++;
                }
            }
            if (count_times>0)
                continue;

            //cut on only 1 hit in 1 bar
            auto count_bars_A = std::vector<int>{0, 0, 0, 0};
            auto count_bars_C = std::vector<int>{0, 0, 0, 0};
            for (size_t j = 0; j < toFHit_bar->size(); j++)
            {
                if (toFHit_stationID->at(j) == 0)
                {
                    size_t k = toFHit_bar->at(j);
                    count_bars_A[k]++;
                }
                if (toFHit_stationID->at(j) == 3)
                {
                    size_t k = toFHit_bar->at(j);
                    count_bars_C[k]++;
                }
            }
            int count_hitsInBars=0;
            for (size_t k = 0; k < 4; k++)              
                if (count_bars_A[k] > 1 || count_bars_C[k] > 1)
                    count_hitsInBars++;
            if (count_hitsInBars>0)
                continue;   


            //prep for all chans tof vertex, separate A and C
            std::vector<float> chans_A, chans_C, times_A(16,-1), times_C(16,-1);
            for (size_t j = 0; j < toFHit_channel->size(); ++j)
            {
                chan = toFHit_channel->at(j);
                if (toFHit_stationID->at(j) == 0)
                {
                    chans_A.push_back(chan);
                    times_A[chan]=toFHit_time->at(j);
                }
                if (toFHit_stationID->at(j) == 3)
                {
                    chans_C.push_back(chan);
                    times_C[chan]=toFHit_time->at(j);
                }
            }

            //all chans, no event mixing for pv-tof
            for (size_t k = 0; k < pv_vertexType->size(); k++)
            {
                if (pv_vertexType->at(k)==1 && chans_A.size()!=0 && chans_C.size()!=0)
                {
                    i_A=0;
                    i_C=0;
                    ver_time_A=0;
                    ver_time_C=0;
                    for (size_t j = 0; j < chans_A.size(); ++j)
                    {
                        i_A=chans_A.at(j);

                        ver_time_A = ver_time_A + ( times_A.at(i_A) - params[0][i_A] );
                    }
                    ver_time_A = ver_time_A/chans_A.size();
                    for (size_t j = 0; j < chans_C.size(); ++j)
                    {
                        i_C=chans_C.at(j);
                        ver_time_C = ver_time_C + ( times_C.at(i_C) - params[1][i_C] );
                    }
                    ver_time_C = ver_time_C/chans_C.size();
                    tof_vertex = (ver_time_C-ver_time_A)*speed_of_light/2*pow(10,-6); 
                    h_pv_atlas->Fill(pv_z->at(k));
                    h_tof_vertex->Fill(tof_vertex);
                    h_atlas_minus_tof->Fill(((pv_z->at(k))-(tof_vertex)));
                    h_atlas_minus_tof_close->Fill(((pv_z->at(k))-(tof_vertex)));
                }
            }
            //clear prep for all chans tof vertex
            chans_A.clear();
            chans_C.clear();
        }
    }

    auto fitter = [&](TH1D *hist, std::string name)
    {
        if (hist->GetEntries() > 0)
        {
            TF1* gaus1 = new TF1("gaus1", "gaus");
            gaus1->SetLineColor(2);
            //TF1 *total = new TF1("total", "gaus(0)+gaus(3)");
            //total->SetLineColor(6);    
            hist->GetXaxis()->SetTitle( (name + " [mm]").c_str());
            hist->GetYaxis()->SetTitle("Events / (3.33 mm)"); 
            TFitResultPtr r = hist->Fit("gaus1","S");
            double sigma = r->Parameters()[2];
            double error = r->Errors()[2];
            double mean = r->Parameters()[1];
            double error_mean = r->Errors()[1];
            std::stringstream stream_s;
            stream_s << std::fixed << std::setprecision(2) << sigma;
            std::string sigma_str = stream_s.str();
            std::stringstream stream_e;
            stream_e << std::fixed << std::setprecision(2) << error;
            std::string error_str = stream_e.str();
            std::stringstream stream_m;
            stream_m << std::fixed << std::setprecision(2) << mean;
            std::string mean_str = stream_m.str();
            std::stringstream stream_em;
            stream_em << std::fixed << std::setprecision(2) << error_mean;
            std::string error_mean_str = stream_em.str();
            
            TLegend* legend = new TLegend(0.62,0.77,0.77,0.92);
            legend->SetBorderSize(0);
            legend->AddEntry(hist,"Data 2022","pe");
            legend->AddEntry(gaus1,("m = (" + mean_str + " #pm " + error_mean_str + ") mm").c_str(),"l");
            legend->AddEntry(gaus1,("#sigma = (" + sigma_str + " #pm " + error_str + ") mm").c_str(),"l");
            legend->SetTextSize(0.04);
            TLatex latex;
            latex.SetNDC();
            latex.SetTextSize(0.04);
            hist->Draw("eX0");
            legend->Draw();
            ATLASLabel_WithoutATLAS(0.2,0.93,"Internal");
            latex.DrawLatex(0.2,0.83,(std::string("Run ") + std::to_string(runNumber)).c_str());
            c1->SaveAs((std::string("atlas_minus_tof_") + std::to_string(runNumber) + std::string("_big.pdf")).c_str());
            c1->Clear();

            delete gaus1;
            delete legend;
        }
    };

    auto double_fitter_for_atlas_tof = [&](TH1D *hist)
    {
        if (hist->GetEntries() > 0)
        {
            TF1 * totalFit = new TF1("totalFit","gaus(0)+gaus(3)", -150,150); 
            totalFit->SetLineColor(2);
            TF1 * gaus1 = new TF1("gaus1","gaus(0)", -150,150);
            gaus1->SetLineColor(4);
            gaus1->SetLineWidth(2);
            gaus1->SetLineStyle(4);
            TF1 * gaus2 = new TF1("gaus2","gaus(3)", -30,30);
            gaus2->SetLineColor(6);
            gaus2->SetLineWidth(2);
            gaus2->SetLineStyle(2);

            totalFit->SetParameter(0, 10000); 
            totalFit->SetParameter(1, 0); 
            totalFit->SetParameter(2, 10); 
            totalFit->SetParameter(3, 2000); 
            totalFit->SetParameter(4, 0); 
            totalFit->SetParameter(5, 50);

            hist->Fit(totalFit,"R");
            Double_t par[6];
            Double_t er[6];
            totalFit->GetParameters(&par[0]);
            er[2]=totalFit->GetParError(2);
            er[5]=totalFit->GetParError(5);

            gaus1->SetParameter(0,par[0]);
            gaus1->SetParameter(1,par[1]);
            gaus1->SetParameter(2,par[2]);
            gaus2->SetParameter(3,par[3]);
            gaus2->SetParameter(4,par[4]);
            gaus2->SetParameter(5,par[5]);


            std::stringstream stream_s1;
            stream_s1 << std::fixed << std::setprecision(2) << par[2];
            std::string sigma1_str = stream_s1.str();
            std::stringstream stream_e1;
            stream_e1 << std::fixed << std::setprecision(2) << er[2];
            std::string error1_str = stream_e1.str();
            std::stringstream stream_m1;
            stream_m1 << std::fixed << std::setprecision(2) << par[1];
            std::string mean1_str = stream_m1.str();
            std::stringstream stream_em1;
            stream_em1 << std::fixed << std::setprecision(2) << er[1];
            std::string error_mean1_str = stream_em1.str();
            std::stringstream stream_num1;
            stream_num1 << std::fixed << std::setprecision(2) << par[0];
            std::string num1_str = stream_num1.str();

            std::stringstream stream_s2;
            stream_s2 << std::fixed << std::setprecision(2) << par[5];
            std::string sigma2_str = stream_s2.str();
            std::stringstream stream_e2;
            stream_e2 << std::fixed << std::setprecision(2) << er[5];
            std::string error2_str = stream_e2.str();
            std::stringstream stream_m2;
            stream_m2 << std::fixed << std::setprecision(2) << par[4];
            std::string mean2_str = stream_m2.str();
            std::stringstream stream_em2;
            stream_em2 << std::fixed << std::setprecision(2) << er[4];
            std::string error_mean2_str = stream_em2.str();
            std::stringstream stream_num2;
            stream_num2 << std::fixed << std::setprecision(2) << par[3];
            std::string num2_str = stream_num2.str();

            hist->GetXaxis()->SetTitle( "z_{ATLAS} - z_{ToF} [mm]" );
            hist->GetYaxis()->SetTitle("Events / (1.66 mm)"); 

            TLegend* legend = new TLegend(0.62,0.77,0.77,0.92);
            legend->SetBorderSize(0);
            legend->AddEntry(hist, "Data 2022", "pe");
            //legend->AddEntry(gaus1, ("M = (" + mean1_str + " #pm " + error_mean1_str + ") mm").c_str(), "l");
            legend->AddEntry(gaus1, ("#sigma = (" + sigma1_str + " #pm " + error1_str + ") mm").c_str(), "l");
            //legend->AddEntry(gaus1, ("N = (" + std::to_string(yield1) + ")").c_str(), "l");
            //legend->AddEntry(gaus2, ("M = (" + mean2_str + " #pm " + error_mean2_str + ") mm").c_str(), "l");
            legend->AddEntry(gaus2, ("#sigma = (" + sigma2_str + " #pm " + error2_str + ") mm").c_str(), "l");
            //legend->AddEntry(gaus2, ("N = (" + std::to_string(yield2) + ")").c_str(), "l");
            legend->SetTextSize(0.04);
            TLatex latex;
            latex.SetNDC();
            latex.SetTextSize(0.04);

            hist->Draw("ex0");
            totalFit->Draw("same"); 
            gaus1->Draw("same"); 
            gaus2->Draw("same");
            legend->Draw();
            ATLASLabel_WithoutATLAS(0.2,0.93,"Internal");
            latex.DrawLatex(0.2,0.83,(std::string("Run ") + std::to_string(runNumber)).c_str());
            c1->SaveAs((std::string("atlas_minus_tof_withoutTr2BC_") + std::to_string(runNumber) + std::string("_big.pdf")).c_str());
            c1->Clear();

            delete totalFit;
            delete gaus1;
            delete gaus2;
            delete legend;
        }
        
    };
    
    auto drawer = [&](TH1D *hist, std::string name)
    {
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        hist->Draw("eX0");
        ATLASLabel_WithoutATLAS(0.2,0.86,"Internal");
        latex.DrawLatex(0.2,0.75,(std::string("Run ") + std::to_string(runNumber)).c_str());
        latex.DrawLatex(0.2,0.7,(name).c_str());
        c1->SaveAs((std::string("atlas_minus_tof_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    };
    
    TFile *outf4 = new TFile("out_ME_atlas_minus_tof_withoutTr2_435229_big.root", "RECREATE");
    h_atlas_minus_tof->Draw();
    h_atlas_minus_tof->Write();  
    outf4->Close();


}