#include <TRandom3.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TFormula.h>
#include <TF1.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TGraph.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

//using namespace std;

int main(int argc, char **argv)
{

    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }

    // setup
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);
    TFile *f = new TFile(argv[1], "READ");

    int runNumber=435229;
    auto n_bin = 200;
    
    TGraph* g_beam_z_lb = (TGraph*)f->Get("g_beam_lb");

    //goodlooking
    g_beam_z_lb->SetMarkerColor(4);

    /*
    //old one
    TH2D* h2_tof_lb_before = (TH2D*)f->Get("2_tof_lb_before_43");
    h2_tof_lb_before->GetXaxis()->SetTitle("Lumiblock");
    h2_tof_lb_before->GetYaxis()->SetTitle("z position [mm]");
    c1->SaveAs((std::string("All_ME_old_tof_bs_") + std::to_string(runNumber) + std::string("_big.pdf[")).c_str());
    h2_tof_lb_before->Draw("colz");
    g_beam_z_lb->Draw("sameP");
    TLegend* legend2 = new TLegend(0.75,0.78,0.9,0.9);
    legend2->SetBorderSize(0);
    legend2->AddEntry(g_beam_z_lb,"z_{BS}","p");
    legend2->AddEntry(h2_tof_lb_before,"z_{ToF} (raw)","f");
    legend2->SetTextSize(0.04);
    legend2->SetFillStyle(0);
    legend2->Draw();
    TLatex latex;
    latex.SetNDC();
    latex.SetTextSize(0.04);
    ATLASLabel_WithoutATLAS(0.19,0.9,"Internal");
    latex.DrawLatex(0.19,0.8,(std::string("Run ") + std::to_string(runNumber)).c_str());
    c1->SaveAs((std::string("All_ME_old_tof_bs_") + std::to_string(runNumber) + std::string("_big.pdf")).c_str());
    c1->Clear();
    c1->SaveAs((std::string("All_ME_old_tof_bs_") + std::to_string(runNumber) + std::string("_big.pdf]")).c_str());
    */

    
    //fitSlices
    TH2D* h2_tof_lb = (TH2D*)f->Get("2_tof_lb_43");
    h2_tof_lb->GetXaxis()->SetTitle("Lumiblock");
    h2_tof_lb->GetYaxis()->SetTitle("z position [mm]");
    TF1 * fg = new TF1("gaussian","gaus(0)", -300,300);
    TObjArray aSlices;
    h2_tof_lb->FitSlicesY(fg, 0, -1, 0, "QNRG3", &aSlices);

    TH1D *h2_tof_lb_1 = (TH1D*)f->Get("2_tof_lb_43_1");
    h2_tof_lb_1->SetMarkerSize(0.8);

    c1->SaveAs((std::string("All_ME_tof_bs_") + std::to_string(runNumber) + std::string("_big.pdf[")).c_str());
    h2_tof_lb->Draw("colz");
    g_beam_z_lb->Draw("sameP");
    h2_tof_lb_1->Draw("same*P");

    TLegend* legend = new TLegend(0.75,0.79,0.9,0.94);
    legend->SetBorderSize(0);
    legend->AddEntry(h2_tof_lb,"z_{ToF} (raw)","f");
    legend->AddEntry(g_beam_z_lb,"z_{BS}","p");
    legend->AddEntry(h2_tof_lb_1,"z_{ToF} (fitting)","pe");
    legend->SetTextSize(0.04);
    legend->SetFillStyle(0);
    TLatex latex;
    latex.SetNDC();
    latex.SetTextSize(0.04);
    legend->Draw();
    ATLASLabel_WithoutATLAS(0.19,0.94,"Internal");
    latex.DrawLatex(0.19,0.84,(std::string("Run ") + std::to_string(runNumber)).c_str());
    c1->SaveAs((std::string("All_ME_tof_bs_") + std::to_string(runNumber) + std::string("_big.pdf")).c_str());
    c1->Clear();

    g_beam_z_lb->GetYaxis()->SetRangeUser(-30, 30);
    g_beam_z_lb->GetXaxis()->SetTitle("Lumiblock");
    g_beam_z_lb->GetYaxis()->SetTitle("z position [mm]");
    g_beam_z_lb->Draw("AP");
    h2_tof_lb_1->Draw("same*P");

    TLegend* legend2 = new TLegend(0.75,0.79,0.9,0.94);
    legend2->SetBorderSize(0);
    legend2->AddEntry(g_beam_z_lb,"z_{BS}","p");
    legend2->AddEntry(h2_tof_lb_1,"z_{ToF} (fitting)","pe");
    legend2->SetTextSize(0.04);
    legend2->SetFillStyle(0);
    legend2->Draw();
    ATLASLabel_WithoutATLAS(0.19,0.94,"Internal");
    latex.DrawLatex(0.19,0.84,(std::string("Run ") + std::to_string(runNumber)).c_str());
    c1->SaveAs((std::string("All_ME_tof_bs_") + std::to_string(runNumber) + std::string("_big.pdf")).c_str());
    c1->Clear();
    
    c1->SaveAs((std::string("All_ME_tof_bs_") + std::to_string(runNumber) + std::string("_big.pdf]")).c_str());
    

}
