#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <limits>

#include <TRandom3.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TFormula.h>
#include <TF1.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TColor.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TLatex.h>
#include <TMath.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

int main(int argc, char **argv)
{

    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }

    // setup
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);
    TFile *inf_sh = new TFile(argv[1], "READ");

    //int runNumber=429142;
    int runNumber=435229;
    auto n_bin = 200;

    auto hists_tof_minus_bs = std::vector<std::vector<TH1D *>>(16, std::vector<TH1D *>(16));
    auto shifts_tof = std::vector<std::vector<float>>(16, std::vector<float>(16));
    auto stations = std::vector<std::string>{"A", "C"};
    auto channels = std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    auto bars = std::vector<std::string>{"A", "B", "C", "D"};
    auto trains = std::vector<std::string>{"0", "1", "2", "3"};

    for (size_t i = 0; i < 16; i++)
        for (size_t j = 0; j < 16; j++)
            hists_tof_minus_bs[i][j] = (TH1D*)inf_sh->Get(("tof_bs" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str());

    auto fitter_simple = [&](TH1D *hist)
    {
        if (hist->GetEntries() > 0)
        {
            TF1* gaus1 = new TF1("gaus1", "gaus");
            gaus1->SetLineColor(2);    
            TFitResultPtr r = hist->Fit("gaus1","S");
            double mean = r->Parameters()[1];
            delete gaus1;

            return mean;
        }
        else
        {return 0.0;}   
    };

    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            shifts_tof[i][j]=fitter_simple(hists_tof_minus_bs[i][j]);


    auto fitter = [&](TH1D *hist, int channel_A, int channel_C)
    {
        if (hist->GetEntries() > 0)
        {
            TF1* gaus1 = new TF1("gaus1", "gaus");
            gaus1->SetLineColor(2);
            //TF1 *total = new TF1("total", "gaus(0)+gaus(3)");
            //total->SetLineColor(6);
            hist->GetXaxis()->SetTitle("z_{ToF} - z_{BS} [mm]");
            hist->GetYaxis()->SetTitle("Events / (6 mm)");  
            TFitResultPtr r = hist->Fit("gaus1","S");
            double sigma = r->Parameters()[2];
            double error = r->Errors()[2];
            double mean = r->Parameters()[1];
            double error_mean = r->Errors()[1];
            std::stringstream stream_s;
            stream_s << std::fixed << std::setprecision(2) << sigma;
            std::string sigma_str = stream_s.str();
            std::stringstream stream_e;
            stream_e << std::fixed << std::setprecision(2) << error;
            std::string error_str = stream_e.str();
            std::stringstream stream_m;
            stream_m << std::fixed << std::setprecision(2) << mean;
            std::string mean_str = stream_m.str();
            std::stringstream stream_em;
            stream_em << std::fixed << std::setprecision(2) << error_mean;
            std::string error_mean_str = stream_em.str();
            
            TLegend* legend = new TLegend(0.58,0.73,0.75,0.88);
            legend->SetBorderSize(0);
            legend->AddEntry(hist,"Data 2022","pe");
            legend->AddEntry(gaus1,("m = (" + mean_str + " #pm " + error_mean_str + ") mm").c_str(),"l");
            legend->AddEntry(gaus1,("#sigma = (" + sigma_str + " #pm " + error_str + ") mm").c_str(),"l");
            legend->SetTextSize(0.04);
            TLatex latex;
            latex.SetNDC();
            latex.SetTextSize(0.04);
            hist->Draw("eX0");
            legend->Draw();
            int barA = channel_A % 4;
            int barC = channel_C % 4;
            int trA = channel_A / 4;
            int trC = channel_C / 4;
            ATLASLabel_WithoutATLAS(0.2,0.9,"Internal");
            latex.DrawLatex(0.2,0.8,(std::string("Run ") + std::to_string(runNumber)).c_str());
            latex.DrawLatex(0.2,0.75,("Side A, " + trains[trA] + bars[barA]).c_str());
            latex.DrawLatex(0.2,0.7,("Side C, " + trains[trC] + bars[barC]).c_str());
            c1->SaveAs((std::string("tof_minus_bs_nice_") + std::to_string(runNumber) + std::string("_big.pdf")).c_str());
            c1->Clear();

            delete gaus1;
            delete legend;
        }
    };
    
    c1->SaveAs((std::string("tof_minus_bs_nice_") + std::to_string(runNumber) + std::string("_big.pdf[")).c_str());
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            fitter(hists_tof_minus_bs[i][j],i,j);
    c1->SaveAs((std::string("tof_minus_bs_nice_") + std::to_string(runNumber) + std::string("_big.pdf]")).c_str());

}