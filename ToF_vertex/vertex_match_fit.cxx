#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <limits>

#include <TRandom3.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TFormula.h>
#include <TF1.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TColor.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TLatex.h>
#include <TMath.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

Double_t myFunc (Double_t *x, Double_t *pars) 
    {
        Float_t xx =x[0];
        Double_t f = 0;
        for (size_t i = 0; i < 16; i++)
            for (size_t j = 0; j < 16; j++)
                f = f + ( ((xx>i*16+j) && (xx<i*16+j+1)) * (pars[16+j]-pars[i]));      
        return f;
    }

int main(int argc, char **argv)
{

    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }

    // setup
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);
    TFile *inf_sh = new TFile(argv[1], "READ");

    int runNumber=435229;

    //auto hist = std::vector<std::vector<TH1D *>>(16, std::vector<TH1D *>(16));
    TH1D* hist = (TH1D*)inf_sh->Get("atlas_minus_tof");
    

    c1->SaveAs((std::string("fit_atlas_minus_tof_withoutTr2_") + std::to_string(runNumber) + std::string("_big.pdf[")).c_str());

    TF1 * totalFit = new TF1("totalFit","gaus(0)+gaus(3)", -150,150); 
    totalFit->SetLineColor(2);
    TF1 * gaus1 = new TF1("gaus1","gaus(0)", -150,150);
    gaus1->SetLineColor(4);
    gaus1->SetLineWidth(2);
    gaus1->SetLineStyle(4);
    TF1 * gaus2 = new TF1("gaus2","gaus(3)", -30,30);
    gaus2->SetLineColor(6);
    gaus2->SetLineWidth(2);
    gaus2->SetLineStyle(2);

    totalFit->SetParameter(0, 10000); 
    totalFit->SetParameter(1, 0); 
    totalFit->SetParameter(2, 10); 
    totalFit->SetParameter(3, 2000); 
    totalFit->SetParameter(4, 0); 
    totalFit->SetParameter(5, 50);

    hist->Fit(totalFit,"SR");
    Double_t par[6];
    Double_t er[6];
    totalFit->GetParameters(&par[0]);
    er[2]=totalFit->GetParError(2);
    er[5]=totalFit->GetParError(5);

    gaus1->SetParameter(0,par[0]);
    gaus1->SetParameter(1,par[1]);
    gaus1->SetParameter(2,par[2]);
    gaus2->SetParameter(3,par[3]);
    gaus2->SetParameter(4,par[4]);
    gaus2->SetParameter(5,par[5]);


    std::stringstream stream_s1;
    stream_s1 << std::fixed << std::setprecision(2) << par[2];
    std::string sigma1_str = stream_s1.str();
    std::stringstream stream_e1;
    stream_e1 << std::fixed << std::setprecision(2) << er[2];
    std::string error1_str = stream_e1.str();
    std::stringstream stream_m1;
    stream_m1 << std::fixed << std::setprecision(2) << par[1];
    std::string mean1_str = stream_m1.str();
    std::stringstream stream_em1;
    stream_em1 << std::fixed << std::setprecision(2) << er[1];
    std::string error_mean1_str = stream_em1.str();

    std::stringstream stream_s2;
    stream_s2 << std::fixed << std::setprecision(2) << par[5];
    std::string sigma2_str = stream_s2.str();
    std::stringstream stream_e2;
    stream_e2 << std::fixed << std::setprecision(2) << er[5];
    std::string error2_str = stream_e2.str();
    std::stringstream stream_m2;
    stream_m2 << std::fixed << std::setprecision(2) << par[4];
    std::string mean2_str = stream_m2.str();
    std::stringstream stream_em2;
    stream_em2 << std::fixed << std::setprecision(2) << er[4];
    std::string error_mean2_str = stream_em2.str();

    hist->GetXaxis()->SetTitle( "z_{ATLAS} - z_{ToF} [mm]" );
    hist->GetYaxis()->SetTitle("Events / (1.66 mm)"); 

    float binWidth = hist->GetXaxis()->GetBinWidth(1);
    float yield1 = gaus1->Integral(-150,150)/binWidth;
    float yield2 = (totalFit->Integral(-150,150)/binWidth) - yield1;
    std::stringstream stream_y2;
    stream_y2 << std::fixed << std::setprecision(0) << yield2;
    std::string y2_str = stream_y2.str();
    std::stringstream stream_y1;
    stream_y1 << std::fixed << std::setprecision(0) << yield1;
    std::string y1_str = stream_y1.str();

    TLegend* legend = new TLegend(0.62,0.67,0.77,0.92);
    legend->SetBorderSize(0);
    legend->AddEntry(hist, "Data 2022", "pe");
    //legend->AddEntry(gaus1, ("M = (" + mean1_str + " #pm " + error_mean1_str + ") mm").c_str(), "l");
    legend->AddEntry(gaus1, ("#sigma = (" + sigma1_str + " #pm " + error1_str + ") mm").c_str(), "l");
    legend->AddEntry(gaus1, ("N = " + y1_str + " events").c_str(), "l");
    //legend->AddEntry(gaus2, ("M = (" + mean2_str + " #pm " + error_mean2_str + ") mm").c_str(), "l");
    legend->AddEntry(gaus2, ("#sigma = (" + sigma2_str + " #pm " + error2_str + ") mm").c_str(), "l");
    legend->AddEntry(gaus2, ("N = " + y2_str + " events").c_str(), "l");
    legend->SetTextSize(0.04);
    TLatex latex;
    latex.SetNDC();
    latex.SetTextSize(0.04);

    hist->Draw("ex0");
    totalFit->Draw("same"); 
    gaus1->Draw("same"); 
    gaus2->Draw("same");
    legend->Draw();
    ATLASLabel_WithoutATLAS(0.2,0.93,"Internal");
    latex.DrawLatex(0.2,0.83,(std::string("Run ") + std::to_string(runNumber)).c_str());
    c1->SaveAs((std::string("fit_atlas_minus_tof_withoutTr2_") + std::to_string(runNumber) + std::string("_big.pdf")).c_str());
    c1->Clear();
    
    
    
    //fitter(h_pv_atlas, "z_{ATLAS}");
    //fitter(h_tof_vertex, "z_{ToF}");
    c1->SaveAs((std::string("fit_atlas_minus_tof_withoutTr2_") + std::to_string(runNumber) + std::string("_big.pdf]")).c_str());


}