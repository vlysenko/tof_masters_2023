#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <limits>

#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TColor.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TRandom3.h>
#include <TMath.h>
#include <TGraph.h>
#include <TMultiGraph.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"


int main(int argc, char **argv)
{
    if (argc != 3)
    {
        std::cout << "Requires 3 parameters\n";
        return 1;
    }

    // setup
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);

    //auto in_files = std::vector<TFile *>(2);
    //in_files[0]=new TFile(argv[1], "READ");
    //in_files[1]=new TFile(argv[2], "READ");

    TFile *inf = new TFile(argv[1], "READ");


    TTree *tree_data = inf->Get<TTree>("CollectionTree");
    size_t nentries = tree_data->GetEntries();

    bool ATLASkey;
    std::string arg1(argv[2]);

    if (arg1=="atlas") {ATLASkey = true;}
    if (arg1=="noatlas") {ATLASkey = false;}

    auto n_bin = 200;
    auto n_bin_tof_lb_42_1=530-320;
    auto n_bin_tof_lb_42_2=200-(-200);
    auto n_bin_tof_lb_43_1=(5000-500)/5;
    auto n_bin_tof_lb_43_2=250-(-200);

    TH2D *h2_tof_lb_42 = new TH2D("2_tof_lb_42", "2_tof_lb_42", n_bin_tof_lb_42_1, 320, 530, n_bin_tof_lb_42_2, -500, 600);
    TH2D *h2_tof_lb_43 = new TH2D("2_tof_lb_43", "2_tof_lb_43", n_bin_tof_lb_43_1, 500, 5000, n_bin_tof_lb_43_2, -300, 400);
    TH2D *h2_tof_lb_before_42 = new TH2D("2_tof_lb_before_42", "2_tof_lb_before_42", n_bin_tof_lb_42_1, 320, 530, n_bin_tof_lb_42_2, -3000, 100);
    TH2D *h2_tof_lb_before_43 = new TH2D("2_tof_lb_before_43", "2_tof_lb_before_43", n_bin_tof_lb_43_1, 500, 5000, n_bin_tof_lb_43_2, -3000, 100);

    std::vector<float> *toFHit_channel = nullptr, *toFHit_trainID = nullptr, *toFHit_stationID = nullptr, 
                       *toFHit_time = nullptr, *toFHit_bar = nullptr, *pv_z = nullptr, *pv_vertexType = nullptr,
                       *aFPTrack_xLocal = nullptr, *aFPTrack_stationID = nullptr;

    int runNumber;
    int eventNumber;
    int lumi_block;
    float beam_pos;

    tree_data->SetBranchAddress("ToFHit_channel", &toFHit_channel);
    tree_data->SetBranchAddress("ToFHit_trainID", &toFHit_trainID);
    tree_data->SetBranchAddress("ToFHit_stationID", &toFHit_stationID);
    tree_data->SetBranchAddress("ToFHit_time", &toFHit_time);
    tree_data->SetBranchAddress("ToFHit_bar", &toFHit_bar);
    tree_data->SetBranchAddress("AFPTrack_xLocal", &aFPTrack_xLocal);
    tree_data->SetBranchAddress("AFPTrack_stationID", &aFPTrack_stationID);
    tree_data->SetBranchAddress("RunNumber", &runNumber);
    tree_data->SetBranchAddress("EventNumber", &eventNumber);
    tree_data->SetBranchAddress("lumiBlock", &lumi_block);
    tree_data->SetBranchAddress("beam_pos_z", &beam_pos);
    tree_data->SetBranchAddress("PV_vertexType", &pv_vertexType);
    tree_data->SetBranchAddress("PV_z", &pv_z);

    float tof_vertex=0;
    float time=0;
    float speed_of_light = TMath::C();
    float tof_minus_bs;

    TGraph *g_beam_z_lb = new TGraph();

    int chan;
    int i_A;
    int i_C;

    tree_data->GetEntry(1);
    int lumi_block_prev=lumi_block;

    auto stations = std::vector<std::string>{"A", "C"};
    auto channels = std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    auto hists_tof_vertex = std::vector<std::vector<TH1D *>>(16, std::vector<TH1D *>(16));
    auto hists_tof_minus_bs = std::vector<std::vector<TH1D *>>(16, std::vector<TH1D *>(16));
    auto hists2D_tof_lb_42 = std::vector<std::vector<TH2D *>>(16, std::vector<TH2D *>(16));
    auto hists2D_tof_lb_43 = std::vector<std::vector<TH2D *>>(16, std::vector<TH2D *>(16));

    auto shifts_tof = std::vector<std::vector<float>>(16, std::vector<float>(16));
    auto buffer_times_A = std::vector<std::vector<float>>(16, std::vector<float>());
    auto buffer_times_C = std::vector<std::vector<float>>(16, std::vector<float>());
    
    for (size_t i = 0; i < 16; i++)
        for (size_t j = 0; j < 16; j++)
        {
            hists_tof_vertex[i][j] = new TH1D(("tof" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), 
            ("tof" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), n_bin, -3200, -2000);
            
            hists_tof_minus_bs[i][j] = new TH1D(("tof_bs" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), 
            ("tof_bs" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), n_bin, -3200, -2000);
            
            hists2D_tof_lb_42[i][j] = new TH2D(("2D_tof_lb_42" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), 
            ("2D_tof_lb_42" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), 
            n_bin_tof_lb_42_1, 320, 530, n_bin_tof_lb_42_2, -500, 500);

            hists2D_tof_lb_43[i][j] = new TH2D(("2D_tof_lb_43" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), 
            ("2D_tof_lb_43" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str(), 
            n_bin_tof_lb_43_1, 500, 5000, n_bin_tof_lb_43_2, -300, 350);
        }
            

    //1st main loop

    for (size_t i = 0; i < nentries; i++)
    //for (size_t i = 0; i < nentries/10; i++)
    {   
        if (runNumber == 429142)
            if (i % 300000 == 0)
                std::cout << i/300000 << " from " << nentries/300000 << "\n";
        if (runNumber == 435229)
            if (i % 9000000 == 0)
                std::cout << i/9000000 << " from " << nentries/9000000 << "\n";

        tree_data->GetEntry(i);

        if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
            continue;

        // cut in only 1 track per event
        int sum_tracks_A = 0, sum_tracks_C = 0;
        for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
        {
            if (aFPTrack_stationID->at(j) == 0)
                sum_tracks_A++;
            if (aFPTrack_stationID->at(j) == 3)
                sum_tracks_C++;
        }
        if (sum_tracks_A != 1 || sum_tracks_C != 1)
            continue;

        //cut on only 1 train per event
        std::vector<int> vec_train_A(4, 0), vec_train_C(4, 0);
        for (size_t j = 0; j < toFHit_trainID->size(); j++)
        {
            for (size_t h = 0; h < 4; h++)
            {
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 0)
                    vec_train_A[h] = 1;
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 3)
                    vec_train_C[h] = 1;
            }
        }
        int sum_train_A = 0, sum_train_C = 0;
        for (size_t h = 0; h < 4; h++)
        {
            sum_train_A += vec_train_A[h];
            sum_train_C += vec_train_C[h];
        }
        if ( (sum_train_A != 1) || (sum_train_C != 1) ) 
            continue;

        //cut on arrival time
        int count_times=0;
        for (size_t count = 0; count < toFHit_channel->size(); ++count)
        {
            time=0;
            if (toFHit_channel->at(count)>=16)
                continue;
            if (toFHit_stationID->at(count) == 0)
            {
                time = toFHit_time->at(count);
                if (1024 / 25 * time > 1000 || 1024 / 25 * time < 900) 
                    count_times++;
            }
            if (toFHit_stationID->at(count) == 3)
            {
                time = toFHit_time->at(count);
                if (1024 / 25 * time > 300 || 1024 / 25 * time < 200) 
                    count_times++;
            }
        }
        if (count_times>0)
            continue;

        //cut on only 1 hit in 1 bar
        auto count_bars_A = std::vector<int>{0, 0, 0, 0};
        auto count_bars_C = std::vector<int>{0, 0, 0, 0};
        for (size_t j = 0; j < toFHit_bar->size(); j++)
        {
            if (toFHit_stationID->at(j) == 0)
            {
                size_t k = toFHit_bar->at(j);
                count_bars_A[k]++;
            }
            if (toFHit_stationID->at(j) == 3)
            {
                size_t k = toFHit_bar->at(j);
                count_bars_C[k]++;
            }
        }
        int count_hitsInBars=0;
        for (size_t k = 0; k < 4; k++)              
            if (count_bars_A[k] > 1 || count_bars_C[k] > 1)
                count_hitsInBars++;
        if (count_hitsInBars>0)
            continue;   
        
        //prep for all chans tof vertex, separate A and C
        std::vector<float> chans_A, chans_C, times_A(16,-1), times_C(16,-1);
        for (size_t j = 0; j < toFHit_channel->size(); ++j)
        {
            chan = toFHit_channel->at(j);
            if (toFHit_stationID->at(j) == 0)
            {
                chans_A.push_back(chan);
                times_A[chan]=toFHit_time->at(j);
            }
            if (toFHit_stationID->at(j) == 3)
            {
                chans_C.push_back(chan);
                times_C[chan]=toFHit_time->at(j);
            }
        }
        
        //graph bs vs lb
        g_beam_z_lb->AddPoint(double(lumi_block),double(beam_pos));
        
        //all chans event mixing: tof vertex, tof-bs 
        if (lumi_block != lumi_block_prev)
        {
            for (size_t j = 0; j < 16; ++j)
                for (size_t h = 0; h < 16; ++h)
                    for (size_t buf_k_A = 0; buf_k_A < buffer_times_A[j].size(); ++buf_k_A)
                        for (size_t buf_k_C = 0; buf_k_C < buffer_times_C[h].size(); ++buf_k_C)
                        {
                            tof_vertex=(buffer_times_C[h].at(buf_k_C)-buffer_times_A[j].at(buf_k_A))*1000*speed_of_light/2*pow(10,-9);
                            hists_tof_vertex[j][h]->Fill(tof_vertex);
                            tof_minus_bs=float(tof_vertex)-float(beam_pos);
                            hists_tof_minus_bs[j][h]->Fill(tof_minus_bs);
                        }
            for (size_t j = 0; j < 16; ++j)
            {
                buffer_times_A[j].clear();
                buffer_times_C[j].clear();
            }
        }
        
        lumi_block_prev=lumi_block;
        for (size_t k = 0; k < chans_A.size(); ++k)
        {
            i_A=chans_A.at(k);
            //if (buffer_times_A[i_A].size()<1000000)
                buffer_times_A[i_A].push_back(times_A.at(i_A));
        }
        for (size_t k = 0; k < chans_C.size(); ++k)
        {
            i_C=chans_C.at(k);
            //if (buffer_times_C[i_C].size()<1000000)
                buffer_times_C[i_C].push_back(times_C.at(i_C));
        }

        //clear prep for all chans tof vertex
        chans_A.clear();
        chans_C.clear();
    }

    auto fitter = [&](TH1D *hist, int channel_A, int channel_C)
    {
        if (hist->GetEntries() > 0)
        {
            TF1* gaus1 = new TF1("gaus1", "gaus");
            gaus1->SetLineColor(2);
            //TF1 *total = new TF1("total", "gaus(0)+gaus(3)");
            //total->SetLineColor(6);    
            TFitResultPtr r = hist->Fit("gaus1","S");
            double sigma = r->Parameters()[2];
            double error = r->Errors()[2];
            double mean = r->Parameters()[1];
            double error_mean = r->Errors()[1];
            std::stringstream stream_s;
            stream_s << std::fixed << std::setprecision(2) << sigma;
            std::string sigma_str = stream_s.str();
            std::stringstream stream_e;
            stream_e << std::fixed << std::setprecision(2) << error;
            std::string error_str = stream_e.str();
            std::stringstream stream_m;
            stream_m << std::fixed << std::setprecision(2) << mean;
            std::string mean_str = stream_m.str();
            std::stringstream stream_em;
            stream_em << std::fixed << std::setprecision(2) << error_mean;
            std::string error_mean_str = stream_em.str();
            
            TLegend* legend = new TLegend(0.58,0.7,0.75,0.85);
            legend->SetBorderSize(0);
            legend->AddEntry(hist,"Data 2022","pe");
            legend->AddEntry(gaus1,("m = (" + mean_str + " #pm " + error_mean_str + ") mm").c_str(),"l");
            legend->AddEntry(gaus1,("#sigma = (" + sigma_str + " #pm " + error_str + ") mm").c_str(),"l");
            legend->SetTextSize(0.04);
            TLatex latex;
            latex.SetNDC();
            latex.SetTextSize(0.04);
            hist->Draw("eX0");
            legend->Draw();
            if (ATLASkey==1) {ATLASLabel(0.2,0.86,"Internal");}
            else {ATLASLabel_WithoutATLAS(0.2,0.86,"Internal");}
            latex.DrawLatex(0.2,0.75,(std::string("Run ") + std::to_string(runNumber)).c_str());
            latex.DrawLatex(0.2,0.7,("A" + std::to_string(channel_A)).c_str());
            latex.DrawLatex(0.2,0.65,("C" + std::to_string(channel_C)).c_str());
            c1->SaveAs((std::string("vert_all_ME_tof_minus_bs_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
            c1->Clear();

            delete gaus1;
            delete legend;
        }
    };

    auto fitter_simple = [&](TH1D *hist)
    {
        if (hist->GetEntries() > 0)
        {
            TF1* gaus1 = new TF1("gaus1", "gaus");
            gaus1->SetLineColor(2);    
            TFitResultPtr r = hist->Fit("gaus1","S");
            double mean = r->Parameters()[1];
            delete gaus1;

            return mean;
        }
        else
        {return 0.0;}   
    };

    /*
    c1->SaveAs((std::string("vert_all_ME_tof_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
        {
            hists_tof_vertex[i][j]->Draw();
            c1->SaveAs((std::string("vert_all_ME_tof_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
            c1->Clear();
        }
    c1->SaveAs((std::string("vert_all_ME_tof_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear(); 
    */
    
    TFile *outf2 = new TFile("out_ME_tof_minus_bs_435229.root", "RECREATE");
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
        {
            hists_tof_minus_bs[i][j]->Draw();
            hists_tof_minus_bs[i][j]->Write();
        }
    outf2->Close();
    
    /*
    c1->SaveAs((std::string("vert_all_ME_tof_minus_bs_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
        {
            fitter(hists_tof_minus_bs[i][j],i,j);
        }
    c1->SaveAs((std::string("vert_all_ME_tof_minus_bs_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();
    */
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            shifts_tof[i][j]=fitter_simple(hists_tof_minus_bs[i][j]);
    
    /*
    //shifts
    TGraph *g_shifts = new TGraph();
    g_shifts->SetName("g_shifts");
    
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            g_shifts->AddPoint(double(i*16+j),double(abs(shifts_tof[i][j])));

    c1->SaveAs((std::string("shifts_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    g_shifts->Draw("AP");
    c1->SaveAs((std::string("shifts_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
    c1->Clear();
    c1->SaveAs((std::string("shifts_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();
    */
    //2nd main loop for new z_tof
    tree_data->GetEntry(1);
    lumi_block_prev=lumi_block;

    for (size_t j = 0; j < 16; ++j)
    {
        buffer_times_A[j].clear();
        buffer_times_C[j].clear();
    }
    
    for (size_t i = 0; i < nentries; i++)
    //for (size_t i = 0; i < nentries/10; i++)
    {   
        if (runNumber == 429142)
            if (i % 300000 == 0)
                std::cout << i/300000 << " from " << nentries/300000 << "\n";
        if (runNumber == 435229)
            if (i % 9000000 == 0)
                std::cout << i/9000000 << " from " << nentries/9000000 << "\n";

        tree_data->GetEntry(i);

        if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
            continue;

        // cut in only 1 track per event
        int sum_tracks_A = 0, sum_tracks_C = 0;
        for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
        {
            if (aFPTrack_stationID->at(j) == 0)
                sum_tracks_A++;
            if (aFPTrack_stationID->at(j) == 3)
                sum_tracks_C++;
        }
        if (sum_tracks_A != 1 || sum_tracks_C != 1)
            continue;

        //cut on only 1 train per event
        std::vector<int> vec_train_A(4, 0), vec_train_C(4, 0);
        for (size_t j = 0; j < toFHit_trainID->size(); j++)
        {
            for (size_t h = 0; h < 4; h++)
            {
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 0)
                    vec_train_A[h] = 1;
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 3)
                    vec_train_C[h] = 1;
            }
        }
        int sum_train_A = 0, sum_train_C = 0;
        for (size_t h = 0; h < 4; h++)
        {
            sum_train_A += vec_train_A[h];
            sum_train_C += vec_train_C[h];
        }
        if ( (sum_train_A != 1) || (sum_train_C != 1) ) 
            continue;

        //cut on arrival time
        int count_times=0;
        for (size_t count = 0; count < toFHit_channel->size(); ++count)
        {
            time=0;
            if (toFHit_channel->at(count)>=16)
                continue;
            if (toFHit_stationID->at(count) == 0)
            {
                time = toFHit_time->at(count);
                if (1024 / 25 * time > 1000 || 1024 / 25 * time < 900) 
                    count_times++;
            }
            if (toFHit_stationID->at(count) == 3)
            {
                time = toFHit_time->at(count);
                if (1024 / 25 * time > 300 || 1024 / 25 * time < 200) 
                    count_times++;
            }
        }
        if (count_times>0)
            continue;

        //cut on only 1 hit in 1 bar
        auto count_bars_A = std::vector<int>{0, 0, 0, 0};
        auto count_bars_C = std::vector<int>{0, 0, 0, 0};
        for (size_t j = 0; j < toFHit_bar->size(); j++)
        {
            if (toFHit_stationID->at(j) == 0)
            {
                size_t k = toFHit_bar->at(j);
                count_bars_A[k]++;
            }
            if (toFHit_stationID->at(j) == 3)
            {
                size_t k = toFHit_bar->at(j);
                count_bars_C[k]++;
            }
        }
        int count_hitsInBars=0;
        for (size_t k = 0; k < 4; k++)              
            if (count_bars_A[k] > 1 || count_bars_C[k] > 1)
                count_hitsInBars++;
        if (count_hitsInBars>0)
            continue;   

        //prep for all chans tof vertex, separate A and C
        std::vector<float> chans_A, chans_C, times_A(16,-1), times_C(16,-1);
        for (size_t j = 0; j < toFHit_channel->size(); ++j)
        {
            chan = toFHit_channel->at(j);
            if (toFHit_stationID->at(j) == 0)
            {
                chans_A.push_back(chan);
                times_A[chan]=toFHit_time->at(j);
            }
            if (toFHit_stationID->at(j) == 3)
            {
                chans_C.push_back(chan);
                times_C[chan]=toFHit_time->at(j);
            }
        }

        //all chans event mixing new tof vertex 
        if (lumi_block != lumi_block_prev)
        {
            for (size_t j = 0; j < 16; ++j)
                for (size_t h = 0; h < 16; ++h)
                    for (size_t buf_k_A = 0; buf_k_A < buffer_times_A[j].size(); ++buf_k_A)
                        for (size_t buf_k_C = 0; buf_k_C < buffer_times_C[h].size(); ++buf_k_C)
                        {
                            tof_vertex=(buffer_times_C[h].at(buf_k_C)-buffer_times_A[j].at(buf_k_A))*1000*speed_of_light/2*pow(10,-9);
                            if (runNumber == 429142)
                            {
                                hists2D_tof_lb_42[j][h]->Fill(lumi_block,tof_vertex-shifts_tof[j][h]);
                                h2_tof_lb_42->Fill(lumi_block,tof_vertex-shifts_tof[j][h]);
                                h2_tof_lb_before_42->Fill(lumi_block,tof_vertex);
                            }  
                            if (runNumber == 435229)
                            {
                                hists2D_tof_lb_43[j][h]->Fill(lumi_block,tof_vertex-shifts_tof[j][h]);
                                h2_tof_lb_43->Fill(lumi_block,tof_vertex-shifts_tof[j][h]);
                                h2_tof_lb_before_43->Fill(lumi_block,tof_vertex);
                            }
                        }
            for (size_t j = 0; j < 16; ++j)
            {
                buffer_times_A[j].clear();
                buffer_times_C[j].clear();
            }
        }
        
        lumi_block_prev=lumi_block;
        for (size_t k = 0; k < chans_A.size(); ++k)
        {
            i_A=chans_A.at(k);
            //if (buffer_times_A[i_A].size()<1000000)
                buffer_times_A[i_A].push_back(times_A.at(i_A));
        }
        for (size_t k = 0; k < chans_C.size(); ++k)
        {
            i_C=chans_C.at(k);
            //if (buffer_times_C[i_C].size()<1000000)
                buffer_times_C[i_C].push_back(times_C.at(i_C));
        }

        //clear prep for all chans tof vertex
        chans_A.clear();
        chans_C.clear();
    }
    
    g_beam_z_lb->SetName("g_beam_lb");
    
    //drawing  
    /*
    c1->SaveAs((std::string("vert_all_ME_new_tof_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
        {
            if (runNumber == 429142)
                hists2D_tof_lb_42[i][j]->Draw("colz");
            if (runNumber == 435229)
                hists2D_tof_lb_43[i][j]->Draw("colz");
            g_beam_z_lb->Draw("sameP");
            c1->SaveAs((std::string("vert_all_ME_new_tof_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
            c1->Clear();
        }
    c1->SaveAs((std::string("vert_all_ME_new_tof_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();
    */
    /*
    c1->SaveAs((std::string("vert2_all_ME_newOne_tof_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    if (runNumber == 429142)
        h2_tof_lb_42->Draw("colz");
    if (runNumber == 435229)
        h2_tof_lb_43->Draw("colz");
    g_beam_z_lb->Draw("sameP");
    c1->SaveAs((std::string("vert2_all_ME_newOne_tof_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
    c1->Clear();
    c1->SaveAs((std::string("vert2_all_ME_newOne_tof_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    */
    /*
    TFile *outf3 = new TFile("out_ME_new_tof_429142.root", "RECREATE");
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
        {
            hists2D_tof_lb_42[i][j]->Draw();
            hists2D_tof_lb_42[i][j]->Write();
            g_beam_z_lb->Draw("same");
            g_beam_z_lb->Write();
        }
    outf3->Close();
    */
    
    if (runNumber == 429142)
    {
        TFile *outf4 = new TFile("out_ME_newOne_tof_429142.root", "RECREATE");
        h2_tof_lb_42->Draw();
        h2_tof_lb_42->Write();  

        g_beam_z_lb->Draw("same");
        g_beam_z_lb->Write();
        outf4->Close();

        TFile *outf41 = new TFile("out_ME_oldOne_tof_429142.root", "RECREATE");
        h2_tof_lb_before_42->Draw();
        h2_tof_lb_before_42->Write();  

        g_beam_z_lb->Draw("same");
        g_beam_z_lb->Write();
        outf41->Close();
    }
    if (runNumber == 435229)
    {
        TFile *outf4 = new TFile("out_ME_newOne_tof_435229.root", "RECREATE");
        h2_tof_lb_43->Draw();
        h2_tof_lb_43->Write();  

        g_beam_z_lb->Draw("same");
        g_beam_z_lb->Write();
        outf4->Close();

        TFile *outf41 = new TFile("out_ME_oldOne_tof_435229.root", "RECREATE");
        h2_tof_lb_before_43->Draw();
        h2_tof_lb_before_43->Write();  

        g_beam_z_lb->Draw("same");
        g_beam_z_lb->Write();
        outf41->Close();
    }
    
}