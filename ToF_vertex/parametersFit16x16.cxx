#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <limits>

#include <TRandom3.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TFormula.h>
#include <TF1.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TColor.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TLatex.h>
#include <TMath.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

Double_t myFunc (Double_t *x, Double_t *pars) 
    {
        Float_t xx =x[0];
        Double_t f = 0;
        for (size_t i = 0; i < 16; i++)
            for (size_t j = 0; j < 16; j++)
                f = f + ( ((xx>i*16+j) && (xx<i*16+j+1)) * (pars[16+j]-pars[i]));      
        return f;
    }

int main(int argc, char **argv)
{

    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }

    // setup
    //SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);
    TFile *inf_sh = new TFile(argv[1], "READ");

    int runNumber=429142;
    //int runNumber=435229;
    auto n_bin = 200;
    float speed_of_light = TMath::C();

    auto hists_tof_minus_bs = std::vector<std::vector<TH1D *>>(16, std::vector<TH1D *>(16));
    auto shifts_tof = std::vector<std::vector<float>>(16, std::vector<float>(16));
    auto stations = std::vector<std::string>{"A", "C"};
    auto channels = std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    auto params = std::vector<std::vector<float>>(2, std::vector<float>(16));

    for (size_t i = 0; i < 16; i++)
        for (size_t j = 0; j < 16; j++)
            hists_tof_minus_bs[i][j] = (TH1D*)inf_sh->Get(("tof_bs" + std::to_string(channels[i]) + "A" + std::to_string(channels[j]) + "C").c_str());

    auto fitter_simple = [&](TH1D *hist)
    {
        if (hist->GetEntries() > 0)
        {
            TF1* gaus1 = new TF1("gaus1", "gaus");
            gaus1->SetLineColor(2);    
            TFitResultPtr r = hist->Fit("gaus1","S");
            double mean = r->Parameters()[1];
            delete gaus1;

            return mean;
        }
        else
        {return 0.0;}   
    };

    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            shifts_tof[i][j]=fitter_simple(hists_tof_minus_bs[i][j]);

    //parametrization
    TH1D *h1_shifts = new TH1D("h1_shifts", "h1_shifts", 256, 0, 256);
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 16; j++)
            h1_shifts->SetBinContent((i*16+j+1), shifts_tof[i][j]);

    //get params
    TF1 * tf_parsmetrazation = new TF1("tf_parsmetrazation",myFunc, 0.1,255.9,32);
    for (int i = 0; i < 32; i++)
        tf_parsmetrazation->SetParameter(i, 1500);

    tf_parsmetrazation->SetNpx(1000);
    tf_parsmetrazation->SetLineColor(2);
    h1_shifts->GetXaxis()->SetTitle("Index");
    h1_shifts->GetYaxis()->SetTitle("D_{C_{j}} - D_{A_{i}} [mm]");
    //c1->SaveAs((std::string("params_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    h1_shifts->Fit(tf_parsmetrazation,"R");
    //h1_shifts->Draw(); 
    //tf_parsmetrazation->Draw("same");

    TLegend* legend = new TLegend(0.65,0.84,0.75,0.94);
    legend->SetBorderSize(0);
    legend->AddEntry(h1_shifts,"Delays, data 2022","l");
    legend->AddEntry(tf_parsmetrazation,"Fit","l");
    legend->SetTextSize(0.04);
    //legend->Draw();

    TLatex latex;
    latex.SetNDC();
    latex.SetTextSize(0.04);
    ATLASLabel_WithoutATLAS(0.2,0.3,"Internal");
    //latex.DrawLatex(0.2,0.2,(std::string("Run ") + std::to_string(runNumber)).c_str());

    //c1->SaveAs((std::string("params_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
    //c1->Clear();
    //c1->SaveAs((std::string("params_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());

    
    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 16; j++)
            params[i][j] = tf_parsmetrazation->GetParameter(i*16+j);   
    
    int Nbin_drawBox = 4;
    TCanvas *c2 = new TCanvas("c2", "c2", 400, 400);
    auto trains = std::vector<std::string>{"0", "1", "2", "3"};
    auto bars = std::vector<std::string>{"A", "B", "C", "D"};
    auto names_for_drawBox = std::vector<std::string>{"A", "C"};
    auto good_names_for_drawBox = std::vector<std::string>{"Side A", "Side C"};

    std::vector<TH2D *> hists2d_for_drawBox(names_for_drawBox.size());

    for (size_t i = 0; i < names_for_drawBox.size(); i++)
        hists2d_for_drawBox[i] = new TH2D((names_for_drawBox[i] + std::string("_h2d")).c_str(), (names_for_drawBox[i] + std::string("_par_2d")).c_str(), Nbin_drawBox, 0, 4, Nbin_drawBox, 0, 4);


    for (size_t general = 0; general < names_for_drawBox.size(); general++)
        for (size_t i = 1; i < 5; i++)
            for (size_t j = 1; j < 5; j++)
            {
                float a=params[general][(4*(i-1))+(j-1)];
                hists2d_for_drawBox[general]->SetBinContent(j, i, a);
            }

    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        for (size_t i = 1; i < 5; i++)
        {
            hists2d_for_drawBox[general]->GetXaxis()->SetBinLabel(i, (bars[i-1]).c_str());
            hists2d_for_drawBox[general]->GetYaxis()->SetBinLabel(i, (trains[i-1]).c_str());
            hists2d_for_drawBox[general]->GetXaxis()->SetLabelSize(0.1);
            hists2d_for_drawBox[general]->GetYaxis()->SetLabelSize(0.1);
            hists2d_for_drawBox[general]->GetXaxis()->SetTitleSize(0.05);
            hists2d_for_drawBox[general]->GetYaxis()->SetTitleSize(0.05);
            hists2d_for_drawBox[general]->SetMarkerSize(2);
        }
        hists2d_for_drawBox[general]->GetYaxis()->SetTitle("Trains");
        hists2d_for_drawBox[general]->GetXaxis()->SetTitle("Bars");
        hists2d_for_drawBox[general]->SetTitle((std::string("Run ")+std::to_string(runNumber) + ", " + good_names_for_drawBox[general]+" [mm]").c_str());
    }
        
    gStyle->SetPalette(55);
    c2->Clear();
    c2->SaveAs((std::string("DrawBox_pars_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        hists2d_for_drawBox[general]->SetStats(0);
        hists2d_for_drawBox[general]->Draw("text colz");
        c2->SaveAs((std::string("DrawBox_pars_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c2->Clear();
    }   
    c2->SaveAs((std::string("DrawBox_pars_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    
    
}