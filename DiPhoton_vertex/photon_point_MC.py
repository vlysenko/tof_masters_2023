import ROOT as root
#import Base
import math, sys
import atlasplots as aplt

#root.gROOT.SetBatch(True)
aplt.set_atlas_style()

file = open(sys.argv[1])
fins = [root.TFile(str.rstrip()) for str in file]
trees = [file.Get("tree") for file in fins]
nEntries = [tree.GetEntries() for tree in trees]

nums=[100,200,300,400,500,600,700,800,900,1000,1200,1400,1600,1800,2000]
#nums=[200,400,800,1600]

Nbin=50
hists_uu = [root.TH1D("method_change_vertex_uu_mc{}".format(num), "Unconv mc{}".format(num), Nbin, -250, 250) for num in nums]
hists_cc = [root.TH1D("method_change_vertex_cc_mc{}".format(num), "Conv mc{}".format(num), Nbin, -250, 250) for num in nums]

#fout = root.TFile('simple100.root', "recreate")
c1 = root.TCanvas('c1', 'c1', 800, 600)

for tree_count, tree in enumerate(trees):
    for i in range (nEntries[tree_count]):
        tree.GetEntry(i)
        nconv_mc=0
        for j in [0,1]:
            if tree.PhotonConvType[j]:
                nconv_mc+=1
        zvertex01=tree.SelectedPVtxP3.z()-tree.TruthPVtxP4.z()
        if (nconv_mc==0):
            hists_uu[tree_count].Fill(zvertex01)
        if (nconv_mc==2):
            hists_cc[tree_count].Fill(zvertex01)


#goodlooking  
for i in range(len(nums)):
    hists_cc[i].GetXaxis().SetTitle("z_{0}-z_{1} [mm]")
    hists_uu[i].GetXaxis().SetTitle("z_{0}-z_{1} [mm]")
    hists_cc[i].GetYaxis().SetTitle("Events")
    hists_uu[i].GetYaxis().SetTitle("Events")
    hists_cc[i].SetStats(0)
    hists_uu[i].SetStats(0)


#set colors for all 
for i in range (0,9):
    hists_cc[i].SetLineColor(i+1)
    hists_uu[i].SetLineColor(i+1)
    
for j in range (i,len(nums)):
    hists_cc[j].SetLineColor(30+j)
    hists_uu[j].SetLineColor(30+j)


#both
legend_cc=root.TLegend(0.18,0.35,0.3,0.92)
legend_uu=root.TLegend(0.18,0.35,0.3,0.92)
for i in range(len(nums)):
    legend_cc.AddEntry(hists_cc[i],"ALP {} [GeV]".format(nums[i]),"l")
    legend_uu.AddEntry(hists_uu[i],"ALP {} [GeV]".format(nums[i]),"l")
    legend_cc.SetBorderSize(0)
    legend_uu.SetBorderSize(0)

for i in range(len(nums)):
    hists_cc[i].SetTitle("Photon pointing method, Converted")
    hists_uu[i].SetTitle("Photon pointing method, Unconverted")

#drawing
hists_cc[3].Draw()
#aplt.atlas_label(0.68, 0.87, "Internal")
latex = root.TLatex()
latex.SetNDC()
latex.DrawLatex(0.68, 0.8,'ALP simulation')
#latex.DrawLatex(0.68, 0.73,'#sqrt{s} = 13.6 TeV')
for j in range(0,3):
    hists_cc[j].Draw("same")
for j in range (len(nums)-1,3,-1):
    hists_cc[j].Draw("same")

#both
legend_cc.Draw()
c1.Print("1method_charge_photonPoint_cc_all.pdf")
c1.Clear()

hists_uu[2].Draw()
#aplt.atlas_label(0.68, 0.87, "Internal")
latex2 = root.TLatex()
latex2.SetNDC()
latex2.DrawLatex(0.68, 0.8,'ALP simulation')
#latex2.DrawLatex(0.68, 0.73,'#sqrt{s} = 13.6 TeV')
for j in range(0,2):
    hists_uu[j].Draw("same")
for j in range (len(nums)-1,2,-1):
    hists_uu[j].Draw("same")
#hists_uu[len(nums)-1].Draw()
#for j in range (len(nums)-1,-1,-1):
#    hists_uu[j].Draw("same")
legend_uu.Draw()
c1.Print("1method_charge_photonPoint_uu_all.pdf")
c1.Clear()

#start of fitting

#goodlooking
for i in range(len(nums)):
    hists_cc[i].SetStats(1)
    hists_uu[i].SetStats(1)
    hists_cc[i].SetLineColor(root.kBlue)
    hists_uu[i].SetLineColor(root.kBlue)
    hists_cc[i].SetTitle("Resolution, photon pointing, Converted, MC mass={}".format(nums[i]))
    hists_uu[i].SetTitle("Resolution, photon pointing, Unconverted, MC mass={}".format(nums[i]))
root.gStyle.SetOptFit()
'''
#fit+drawing
g1s=[root.TF1("m1{}".format(num),"gaus",-30,30) for num in nums]
g2s=[root.TF1("m2{}".format(num),"gaus",-150,150) for num in nums]
c1.SaveAs("Method_charge_fit_all.pdf[")
for i in range(len(nums)):
    hists_cc[i].Fit(g1s[i],"R")
    hists_cc[i].Fit(g2s[i],"R+")
    hists_uu[i].Fit("gaus")
    hists_uu[i].Draw()
    c1.SaveAs("Method_charge_fit_all.pdf")
    c1.Clear()
    hists_cc[i].Draw()
    c1.SaveAs("Method_charge_fit_all.pdf")
    c1.Clear()
c1.SaveAs("Method_charge_fit_all.pdf]")
'''