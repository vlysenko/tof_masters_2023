import ROOT as root
import numpy as np
import sys
import os
import tqdm

c_sp_of_light=root.TMath.C()
sigmaA=c_sp_of_light*20e-12*1e3
sigmaC=c_sp_of_light*26e-12*1e3
sigmaAC=np.sqrt(sigmaA*sigmaA+sigmaC*sigmaC)

print(sigmaA)
print(sigmaC)
print(sigmaAC)

filename_mc=sys.argv[1]
filename_data=sys.argv[2]
f_mc=root.TFile.Open(filename_mc,'READ')
f_data=root.TFile.Open(filename_data,'READ')
#f_mc=root.TFile.Open('/eos/user/v/vlysenko/MC_g200_m100-2000/hist-sample400.root','READ')
#f_data=root.TFile.Open('/eos/user/v/vlysenko/data/reroot_data_ph_calo_pointing.root','READ')

tree_mc=f_mc.Get('CollectionTree')
tree_data=f_data.Get('CollectionTree')

nEn_mc=tree_mc.GetEntries()
nEn_data=tree_data.GetEntries()

sum1_uu=0.0
sum2_uu=0.0
sum3_uu=0.0
sum1_cc=0.0
sum2_cc=0.0
sum3_cc=0.0

for i in tqdm.tqdm(range(nEn_mc)):
    tree_mc.GetEntry(i)
    if (len(tree_mc.ph_calo_z)>1):
        nconv_mc=0
        for j in [0,1]:
            if tree_mc.ph_convType[j]:
                nconv_mc+=1
        if (tree_mc.ph_calo_z[0]>-200 and tree_mc.ph_calo_z[0]<200 and tree_mc.ph_calo_z[1]>-200 and tree_mc.ph_calo_z[1]<200):
            PVz_mc=(tree_mc.ph_calo_z[0]+tree_mc.ph_calo_z[1])*0.5

            kolvo1_uu=0
            kolvo2_uu=0
            kolvo3_uu=0
            kolvo1_cc=0
            kolvo2_cc=0
            kolvo3_cc=0

            for j in range(nEn_data):
                tree_data.GetEntry(j)
                if (len(tree_data.ph_calo_z)>1):
                    nconv_data=0
                    for j in [0,1]:
                        if tree_data.ph_convType[j]:
                            nconv_data+=1
                    if (tree_data.ph_calo_z[0]>-200 and tree_data.ph_calo_z[0]<200 and tree_data.ph_calo_z[1]>-200 and tree_data.ph_calo_z[1]<200):
                        PVz_data=(tree_data.ph_calo_z[0]+tree_data.ph_calo_z[1])*0.5
                        if ( (PVz_data>PVz_mc-sigmaAC) and (PVz_data<PVz_mc+sigmaAC) ):
                            if (nconv_data==0 and nconv_mc==0):
                                kolvo1_uu=kolvo1_uu+1
                            if (nconv_data==2 and nconv_mc==2):
                                kolvo1_cc=kolvo1_cc+1
                        if ( (PVz_data>PVz_mc-2*sigmaAC) and (PVz_data<PVz_mc+2*sigmaAC) ):
                            if (nconv_data==0 and nconv_mc==0):
                                kolvo2_uu=kolvo2_uu+1
                            if (nconv_data==2 and nconv_mc==2):
                                kolvo2_cc=kolvo2_cc+1
                        if ( (PVz_data>PVz_mc-3*sigmaAC) and (PVz_data<PVz_mc+3*sigmaAC) ):
                            if (nconv_data==0 and nconv_mc==0):
                                kolvo3_uu=kolvo3_uu+1
                            if (nconv_data==2 and nconv_mc==2):
                                kolvo3_cc=kolvo3_cc+1

            if (kolvo1_uu!=0):
                sum1_uu=sum1_uu+nEn_data/kolvo1_uu
            if (kolvo2_uu!=0):
                sum2_uu=sum2_uu+nEn_data/kolvo2_uu
            if (kolvo3_uu!=0):
                sum3_uu=sum3_uu+nEn_data/kolvo3_uu
            if (kolvo1_cc!=0):
                sum1_cc=sum1_cc+nEn_data/kolvo1_cc
            if (kolvo2_cc!=0):
                sum2_cc=sum2_cc+nEn_data/kolvo2_cc
            if (kolvo3_cc!=0):
                sum3_cc=sum3_cc+nEn_data/kolvo3_cc

ser1_uu=sum1_uu/nEn_mc
ser2_uu=sum2_uu/nEn_mc
ser3_uu=sum3_uu/nEn_mc
ser1_cc=sum1_cc/nEn_mc
ser2_cc=sum2_cc/nEn_mc
ser3_cc=sum3_cc/nEn_mc

print('data rej factor uu 1 = ', ser1_uu)
print('data rej factor uu 2 = ', ser2_uu)
print('data rej factor uu 3 = ', ser3_uu)
print('data rej factor cc 1 = ', ser1_cc)
print('data rej factor cc 2 = ', ser2_cc)
print('data rej factor cc 3 = ', ser3_cc)
