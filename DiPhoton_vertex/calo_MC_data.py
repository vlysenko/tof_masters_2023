import ROOT as root
#import Base
import math, sys
import tqdm
import atlasplots as aplt

#root.gROOT.SetBatch(True)
aplt.set_atlas_style()

#filename_mc=sys.argv[1]
#filename_data=sys.argv[2]

#fin_data=root.TFile("/eos/user/v/vlysenko/data17_sep/user.vlysenko.00329835._000001.hist-output.root", "read")
#fin_mc=root.TFile("/eos/user/v/vlysenko/MC_g200_m100-2000/hist-sample400.root", "read")
fin_mc=root.TFile(sys.argv[1], "read")
fin_data=root.TFile(sys.argv[2], "read")

tree_data = fin_data.Get("CollectionTree")
tree_mc = fin_mc.Get("CollectionTree")
nEntries_data=tree_data.GetEntries()
nEntries_mc=tree_mc.GetEntries()

Nbin=50
#fout = root.TFile('simple100.root', "recreate")
c1 = root.TCanvas('c1', 'c1', 800, 600)

h_ph01_vertex_mc_uu = root.TH1D("ph01_vertex_mc_uu", "ph_both_vertex", Nbin, -250, 250)
h_ph01_vertex_mc_cc = root.TH1D("ph01_vertex_mc_cc", "ph_both_vertex", Nbin, -250, 250)
h_ph01_vertex_data_uu = root.TH1D("ph01_vertex_data_uu", "ph_both_vertex DS835", Nbin, -250, 250)
h_ph01_vertex_data_cc = root.TH1D("ph01_vertex_data_cc", "ph_both_vertex DS835", Nbin, -250, 250)

#define a difference between vertices in MC(mean value-true) and data(photon0-photon1)

for i in range(nEntries_mc):
    tree_mc.GetEntry(i)
    if (len(tree_mc.ph_calo_z)>1):
        nconv_mc=0
        for j in [0,1]:
            if tree_mc.ph_convType[j]:
                nconv_mc+=1
        #if (tree_mc.ph_calo_z[0]>-200 and tree_mc.ph_calo_z[0]<200 and tree_mc.ph_calo_z[1]>-200 and tree_mc.ph_calo_z[1]<200):
        if (tree_mc.ph_calo_z[0]>-200 and tree_mc.ph_calo_z[0]<200 and tree_mc.ph_calo_z[1]>-200 and tree_mc.ph_calo_z[1]<200 and tree_mc.ph_pt[0]>40 and tree_mc.ph_pt[1]>40):
            zvertex01=(tree_mc.ph_calo_z[0]+tree_mc.ph_calo_z[1])*0.5-tree_mc.tru_PV_z
            if (nconv_mc==0):
                h_ph01_vertex_mc_uu.Fill(zvertex01)
            if (nconv_mc==2):
                h_ph01_vertex_mc_cc.Fill(zvertex01)

            
for i in tqdm.tqdm(range(nEntries_data)):
    tree_data.GetEntry(i)
    if (len(tree_data.ph_calo_z)>1):
        nconv_data=0
        for j in [0,1]:
            if tree_data.ph_convType[j]:
                nconv_data+=1
        #if (tree_data.ph_calo_z[0]>-200 and tree_data.ph_calo_z[0]<200 and tree_data.ph_calo_z[1]>-200 and tree_data.ph_calo_z[1]<200):
        if (tree_data.ph_calo_z[0]>-200 and tree_data.ph_calo_z[0]<200 and tree_data.ph_calo_z[1]>-200 and tree_data.ph_calo_z[1]<200 and tree_data.ph_pt[0]>40 and tree_data.ph_pt[1]>40):
            zvertex01=(tree_data.ph_calo_z[0]-tree_data.ph_calo_z[1])
            if (nconv_data==0):
                h_ph01_vertex_data_uu.Fill(zvertex01)
            if (nconv_data==2):
                h_ph01_vertex_data_cc.Fill(zvertex01)

#goodlooking            
h_ph01_vertex_data_uu.GetXaxis().SetTitle("z_{0}-z_{1} [mm]")
h_ph01_vertex_data_uu.GetYaxis().SetTitle("Events")
h_ph01_vertex_data_uu.SetTitle("Difference between 2 photons, unconverted, vertex z-position")
h_ph01_vertex_data_cc.GetXaxis().SetTitle("z_{0}-z_{1} [mm]")
h_ph01_vertex_data_cc.GetYaxis().SetTitle("Events")
h_ph01_vertex_data_cc.SetTitle("Difference between 2 photons, converted, vertex z-position")

h_ph01_vertex_mc_uu.GetXaxis().SetTitle("z_{0}-z_{1} [mm]")
h_ph01_vertex_mc_uu.GetYaxis().SetTitle("Events")
h_ph01_vertex_mc_uu.SetTitle("Difference between 2 photons, unconverted, vertex z-position")
h_ph01_vertex_mc_cc.GetXaxis().SetTitle("z_{0}-z_{1} [mm]")
h_ph01_vertex_mc_cc.GetYaxis().SetTitle("Events")
h_ph01_vertex_mc_cc.SetTitle("Difference between 2 photons, converted, vertex z-position")

h_ph01_vertex_mc_uu.SetStats(0)
h_ph01_vertex_data_uu.SetStats(0)
h_ph01_vertex_mc_cc.SetStats(0)
h_ph01_vertex_data_cc.SetStats(0)

legend01_uu=root.TLegend(0.2,0.8,0.4,0.9)
legend01_uu.SetBorderSize(0)
legend01_uu.AddEntry(h_ph01_vertex_data_uu, "Data 2017","pe")
legend01_uu.AddEntry(h_ph01_vertex_mc_uu, "Simulation", "l")
legend01_cc=root.TLegend(0.2,0.8,0.4,0.9)
legend01_cc.SetBorderSize(0)
legend01_cc.AddEntry(h_ph01_vertex_data_cc, "Data 2017","pe")
legend01_cc.AddEntry(h_ph01_vertex_mc_cc, "Simulation", "l")

h_ph01_vertex_mc_uu.SetLineColor(root.kRed)
h_ph01_vertex_mc_cc.SetLineColor(root.kRed)

#drawing
h_ph01_vertex_mc_uu.DrawNormalized()
h_ph01_vertex_data_uu.DrawNormalized("eX0 same")
legend01_uu.Draw()
#aplt.atlas_label(0.65, 0.87, "Internal")
p = root.TLatex()
p.SetNDC()
#p.DrawLatex(0.65, 0.8,'Data #sqrt{s} = 13.6 TeV')
c1.Print("1vertex_unconv_with40ptCut.pdf")
#c1.Print("vertex_unconv.png")
c1.Clear()
h_ph01_vertex_mc_cc.DrawNormalized()
h_ph01_vertex_data_cc.DrawNormalized("eX0 same")
legend01_cc.Draw()
#aplt.atlas_label(0.65, 0.87, "Internal")
#p.DrawLatex(0.65, 0.8,'Data #sqrt{s} = 13.6 TeV')
c1.Print("1vertex_conv_with40ptCut.pdf")
#c1.Print("vertex_conv.png")
c1.Clear()
'''
#fitting
h_ph01_vertex_mc_uu.SetLineColor(root.kBlue)
h_ph01_vertex_mc_cc.SetLineColor(root.kBlue)

h_ph01_vertex_mc_uu.SetStats(1)
h_ph01_vertex_data_uu.SetStats(1)
h_ph01_vertex_mc_cc.SetStats(1)
h_ph01_vertex_data_cc.SetStats(1)
root.gStyle.SetOptFit()

h_ph01_vertex_mc_uu.Fit("gaus")
h_ph01_vertex_data_uu.Fit("gaus")
h_ph01_vertex_mc_cc.Fit("gaus")
h_ph01_vertex_data_cc.Fit("gaus")

h_ph01_vertex_mc_uu.Draw()
#c1.Print("vertex_unconv_with40ptCut_mc_fit.png")
c1.Print("vertex_unconv_mc_fit.png")
c1.Clear()
h_ph01_vertex_mc_cc.Draw()
#c1.Print("vertex_conv_with40ptCut_mc_fit.png")
c1.Print("vertex_conv_mc_fit.png")
c1.Clear()
h_ph01_vertex_data_uu.Draw()
#c1.Print("vertex_unconv_with40ptCut_data_fit.png")
c1.Print("vertex_unconv_data_fit.png")
c1.Clear()
h_ph01_vertex_data_cc.Draw()
#c1.Print("vertex_conv_with40ptCut_data_fit.png")
c1.Print("vertex_conv_data_fit.png")
c1.Clear()
'''