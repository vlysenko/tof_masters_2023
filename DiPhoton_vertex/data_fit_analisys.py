import ROOT as root
import math, sys
import tqdm
import atlasplots as aplt

#root.gROOT.SetBatch(True)
aplt.set_atlas_style()

file = open(sys.argv[1])
fins = [root.TFile(str.rstrip()) for str in file]
trees = [file.Get("CollectionTree") for file in fins]
nEntries = [tree.GetEntries() for tree in trees]

#nums = [str.split(".")[0].split("e")[1] for str in fins]
nums=[329716,329835,330025,330074,330079,330160,334264,334317,334890,334907,334993,335016,335083,335170,335282,335290,336567,336782,336832]

Nbin=50
hists = [root.TH1D("ph01_vertex_DSs{}".format(num), "DS00{}".format(num), Nbin, -250, 250) for num in nums]

#fout = root.TFile('simple100.root', "recreate")
c1 = root.TCanvas('c1', 'c1', 800, 600)

for tree_count, tree in enumerate(trees):
    if (tree_count%5==0):
        print(tree_count)
    for i in range (nEntries[tree_count]):
        tree.GetEntry(i)
        if (len(tree.ph_calo_z)>1):
            if (tree.ph_calo_z[0]>-200 and tree.ph_calo_z[0]<200 and tree.ph_calo_z[1]>-200 and tree.ph_calo_z[1]<200):
                zvertex01=(tree.ph_calo_z[0]-tree.ph_calo_z[1])
                hists[tree_count].Fill(zvertex01)
'''
for i in range (0,tree_count+1):
    hists[i].Fit("gaus")
    hists[i].SetTitle("DS00{}".format(nums[i]))
    hists[i].Draw()
    c1.Print("data_DS00{}.png".format(nums[i]))
    c1.Clear()
'''
fout = root.TFile('DSs.root', "recreate")
for i in range (0,tree_count+1):
    hists[i].SetStats(0)
    hists[i].Draw()
    hists[i].Write()

fout.Close()
