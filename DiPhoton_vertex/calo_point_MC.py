import ROOT as root
#import Base
import math, sys
import atlasplots as aplt

#root.gROOT.SetBatch(True)
aplt.set_atlas_style()

file = open(sys.argv[1])
fins = [root.TFile(str.rstrip()) for str in file]
trees = [file.Get("CollectionTree") for file in fins]
nEntries = [tree.GetEntries() for tree in trees]

nums=[100,200,300,400,500,600,700,800,900,1000,1200,1400,1600,1800,2000]
#nums=[200,400,800,1600]

Nbin=50
hists_uu = [root.TH1D("method_calo_vertex_uu_mc{}".format(num), "Unconv mc{}".format(num), Nbin, -250, 250) for num in nums]
hists_cc = [root.TH1D("method_calo_vertex_cc_mc{}".format(num), "Conv mc{}".format(num), Nbin, -250, 250) for num in nums]

#fout = root.TFile('simple100.root', "recreate")
c1 = root.TCanvas('c1', 'c1', 800, 600)

for tree_count, tree in enumerate(trees):
    for i in range (nEntries[tree_count]):
        tree.GetEntry(i)
        if (len(tree.ph_calo_z)>1):
            nconv_mc=0
            for j in [0,1]:
                if tree.ph_convType[j]:
                    nconv_mc+=1
            if (tree.ph_calo_z[0]>-200 and tree.ph_calo_z[0]<200 and tree.ph_calo_z[1]>-200 and tree.ph_calo_z[1]<200 and tree.ph_pt[0]>40 and tree.ph_pt[1]>40):
                zvertex01=(tree.ph_calo_z[0]+tree.ph_calo_z[1])*0.5-tree.tru_PV_z
                if (nconv_mc==0):
                    hists_uu[tree_count].Fill(zvertex01)
                if (nconv_mc==2):
                    hists_cc[tree_count].Fill(zvertex01)


#goodlooking  
for i in range(len(nums)):
    hists_cc[i].GetXaxis().SetTitle("z_{0}-z_{1} [mm]")
    hists_uu[i].GetXaxis().SetTitle("z_{0}-z_{1} [mm]")
    hists_cc[i].GetYaxis().SetTitle("Events")
    hists_uu[i].GetYaxis().SetTitle("Events")
    hists_cc[i].SetStats(0)
    hists_uu[i].SetStats(0)

#set colors for all
for i in range (0,9):
    hists_cc[i].SetLineColor(i+1)
    hists_uu[i].SetLineColor(i+1)
    
for j in range (i,len(nums)):
    hists_cc[j].SetLineColor(30+j)
    hists_uu[j].SetLineColor(30+j)


#both
legend_cc=root.TLegend(0.18,0.35,0.3,0.92)
legend_uu=root.TLegend(0.18,0.35,0.3,0.92)
for i in range(len(nums)):
    legend_cc.AddEntry(hists_cc[i],"{} [GeV]".format(nums[i]),"l")
    legend_uu.AddEntry(hists_uu[i],"{} [GeV]".format(nums[i]),"l")

for i in range(len(nums)):
    hists_cc[i].SetTitle("Calo pointing method, Converted")
    hists_uu[i].SetTitle("Calo pointing method, Unconverted")

#drawing
hists_cc[6].Draw()
#aplt.atlas_label(0.68, 0.87, "Internal")
latex = root.TLatex()
latex.SetNDC()
latex.DrawLatex(0.68, 0.8,'ALP simulation')
#latex.DrawLatex(0.68, 0.73,'#sqrt{s} = 13.6 TeV')
for j in range(0,6):
    hists_cc[j].Draw("same")
for j in range (len(nums)-1,6,-1):
    hists_cc[j].Draw("same")

#both
legend_cc.Draw()
c1.Print("1method_calo_cc.pdf")
c1.Clear()

hists_uu[len(nums)-1].Draw()
#aplt.atlas_label(0.68, 0.87, "Internal")
latex2 = root.TLatex()
latex2.SetNDC()
latex2.DrawLatex(0.68, 0.8,'ALP simulation')
#latex2.DrawLatex(0.68, 0.73,'#sqrt{s} = 13.6 TeV')
for j in range (len(nums)-1,-1,-1):
    hists_uu[j].Draw("same")
legend_uu.Draw()
c1.Print("1method_calo_uu.pdf")
c1.Clear()
'''
#start of fitting

#goodlooking
for i in range(len(nums)):
    hists_cc[i].SetStats(1)
    hists_uu[i].SetStats(1)
    hists_cc[i].SetLineColor(root.kBlue)
    hists_uu[i].SetLineColor(root.kBlue)
    hists_cc[i].SetTitle("Resolution, method calo, Converted, MC mass={}".format(nums[i]))
    hists_uu[i].SetTitle("Resolution, method calo, Unconverted, MC mass={}".format(nums[i]))
root.gStyle.SetOptFit()

#fit+drawing
c1.SaveAs("Method_calo_fit.pdf[")
for i in range(len(nums)):
    hists_cc[i].Fit("gaus")
    hists_uu[i].Fit("gaus")
    hists_cc[i].Draw()
    c1.SaveAs("Method_calo_fit.pdf")
    c1.Clear()
    hists_uu[i].Draw()
    c1.SaveAs("Method_calo_fit.pdf")
    c1.Clear()
c1.SaveAs("Method_calo_fit.pdf]")
'''