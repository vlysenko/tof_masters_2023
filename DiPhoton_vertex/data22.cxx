#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <limits>

#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TColor.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TRandom3.h>
#include <TMath.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"


int main(int argc, char **argv)
{
    if (argc != 3)
    {
        std::cout << "Requires 3 parameters\n";
        return 1;
    }

    // setup
    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);
    TFile *inf = new TFile(argv[1], "READ");
    TTree *tree_data = inf->Get<TTree>("CollectionTree");
    size_t nentries = tree_data->GetEntries();

    bool ATLASkey;
    std::string arg1(argv[2]);

    if (arg1=="atlas") {ATLASkey = true;}
    if (arg1=="noatlas") {ATLASkey = false;}

    auto n_bin = 150;
    auto n_bin_tof = 300;

    auto names = std::vector<std::string>{"Unconverted photons", "Converted photons", "Protons ToF", "Protons ATLAS", "Protons ATLAS-ToF"};
    auto names_who_are_you = std::vector<std::string>{"Resolution", "Position", "Correlation", ""};

    TH1D *h_vertex_data_uu = new TH1D("vertex_data_uu", "vertex_data_uu", n_bin, -3000, 3000);
    TH1D *h_vertex_data_cc = new TH1D("vertex_data_cc", "vertex_data_cc", n_bin, -3000, 3000);
    TH1D *h_position_uu = new TH1D("position_uu", "position_uu", n_bin, -1500, 1500);
    TH1D *h_position_cc = new TH1D("position_cc", "position_cc", n_bin, -1500, 1500);
    TH1D *h_tof_vertex = new TH1D("vertex_tof", "vertex_tof", n_bin_tof, -3.5, -1.5);
    TH2D *h_corr_phot_tof_uu = new TH2D("corr_phot_tof_uu", "corr_phot_tof_uu", n_bin, -1500, 1500, n_bin_tof, -3.5, -0.8);
    TH2D *h_corr_phot_tof_cc = new TH2D("corr_phot_tof_cc", "corr_phot_tof_cc", n_bin, -1500, 1500, n_bin_tof, -3.5, -0.8);
    TH1D *h_pv_atlas = new TH1D("pv_atlas", "pv_atlas", n_bin, -250, 250);
    TH1D *h_atlas_minus_tof_150 = new TH1D("atlas_minus_tof", "atlas_minus_tof", 150, -250, 250);
    TH1D *h_atlas_minus_tof_200 = new TH1D("atlas_minus_tof", "atlas_minus_tof", 200, -200, 200);

    std::vector<float> *toFHit_channel = nullptr, *calo_z = nullptr, *toFHit_trainID = nullptr,
                       *toFHit_stationID = nullptr, *conv_type = nullptr, *toFHit_time = nullptr,
                       *toFHit_bar = nullptr, *pv_z = nullptr, *pv_vertexType = nullptr;

    int runNumber;
    int eventNumber;

    tree_data->SetBranchAddress("ToFHit_channel", &toFHit_channel);
    tree_data->SetBranchAddress("ToFHit_trainID", &toFHit_trainID);
    tree_data->SetBranchAddress("ToFHit_stationID", &toFHit_stationID);
    tree_data->SetBranchAddress("ToFHit_time", &toFHit_time);
    tree_data->SetBranchAddress("ToFHit_bar", &toFHit_bar);
    tree_data->SetBranchAddress("RunNumber", &runNumber);
    tree_data->SetBranchAddress("EventNumber", &eventNumber);
    tree_data->SetBranchAddress("ph_calo_z", &calo_z);
    tree_data->SetBranchAddress("ph_convType", &conv_type);
    tree_data->SetBranchAddress("PV_vertexType", &pv_vertexType);
    tree_data->SetBranchAddress("PV_z", &pv_z);

    int nconv_data=0;
    float zvertex01=0;
    float position_phot=0;
    float tof_vertex=0;
    float time=0;
    float atlas_minus_tof=0;
    float speed_of_light = TMath::C();

    //start

    for (size_t i = 0; i < nentries; i++)
    //for (size_t i = 0; i < nentries/10; i++)
    {
        if (i % 30000 == 0)
            std::cout << i/30000 << " from " << nentries/30000 << "\n";
        
        tree_data->GetEntry(i);

        //cut on 2 photons
        if ( (conv_type->size()) != 2 )
            continue;

        //diphotons vertex stuff
        nconv_data=0;
        for (size_t j=0; j<2; j++)
            if (conv_type->at(j))
                nconv_data++;
        if (calo_z->at(0)>-2000 && calo_z->at(0)<2000 && calo_z->at(1)>-2000 && calo_z->at(1)<2000 && calo_z->at(1)!=0 && calo_z->at(0)!=0)
        {
            zvertex01=(calo_z->at(0)-calo_z->at(1));
            if (nconv_data==0)
                h_vertex_data_uu->Fill(zvertex01);
            if (nconv_data==2)
                h_vertex_data_cc->Fill(zvertex01);
        }

    }

    auto double_fitter = [&](TH1D *hist, std::string name, std::string name_who, int gausRangeDown, int gausRangeUp, int gaus1mean, int gaus1width, int gaus2mean, int gaus2width)
    {
        TF1 * totalFit = new TF1("totalFit","gaus(0)+gaus(3)", gausRangeDown,gausRangeUp); 
        totalFit->SetLineColor(2);
        TF1 * gaus1 = new TF1("gaus1","gaus(0)", gausRangeDown,gausRangeUp);
        gaus1->SetLineColor(4);
        gaus1->SetLineWidth(2);
        gaus1->SetLineStyle(4);
        TF1 * gaus2 = new TF1("gaus2","gaus(3)", gausRangeDown,gausRangeUp);
        gaus2->SetLineColor(6);
        gaus2->SetLineWidth(2);
        gaus2->SetLineStyle(2);

        
        totalFit->SetParameter(0, 10000); 
        totalFit->SetParameter(1, gaus1mean); 
        totalFit->SetParameter(2, gaus1width); 
        totalFit->SetParameter(3, 16000); 
        totalFit->SetParameter(4, gaus2mean); 
        totalFit->SetParameter(5, gaus2width);

        hist->Fit(totalFit,"R");
        Double_t par[6];
        Double_t er[6];
        totalFit->GetParameters(&par[0]);
        er[2]=totalFit->GetParError(2);
        er[5]=totalFit->GetParError(5);

        gaus1->SetParameter(0,par[0]);
        gaus1->SetParameter(1,par[1]);
        gaus1->SetParameter(2,par[2]);
        gaus2->SetParameter(3,par[3]);
        gaus2->SetParameter(4,par[4]);
        gaus2->SetParameter(5,par[5]);

        std::stringstream stream_s1;
        stream_s1 << std::fixed << std::setprecision(2) << par[2];
        std::string sigma1_str = stream_s1.str();
        std::stringstream stream_e1;
        stream_e1 << std::fixed << std::setprecision(2) << er[2];
        std::string error1_str = stream_e1.str();
        std::stringstream stream_s2;
        stream_s2 << std::fixed << std::setprecision(2) << par[5];
        std::string sigma2_str = stream_s2.str();
        std::stringstream stream_e2;
        stream_e2 << std::fixed << std::setprecision(2) << er[5];
        std::string error2_str = stream_e2.str();
        TLegend* legend = new TLegend(0.57,0.78,0.77,0.92);
        legend->SetBorderSize(0);
        legend->AddEntry(hist, "Data 2022", "pe");
        legend->AddEntry(gaus1, ("#sigma = (" + sigma1_str + " #pm " + error1_str + ") mm").c_str(), "l");
        legend->AddEntry(gaus2, ("#sigma = (" + sigma2_str + " #pm " + error2_str + ") mm").c_str(), "l");
        legend->SetTextSize(0.04);
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);

        hist->Draw("eX0");
        totalFit->Draw("same"); 
        gaus1->Draw("same"); 
        gaus2->Draw("same");
        legend->Draw();
        if (ATLASkey==1) {ATLASLabel(0.2,0.92,"Internal");}
        else {ATLASLabel_WithoutATLAS(0.2,0.92,"Internal");}
        latex.DrawLatex(0.2,0.82,(std::string("Run ") + std::to_string(runNumber)).c_str());
        latex.DrawLatex(0.2,0.78,(name).c_str());
        latex.DrawLatex(0.2,0.72,(name_who).c_str());
        c1->SaveAs((std::string("vertices_noATLAS_noTrig_filteredEvents_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();

        delete totalFit;
        delete gaus1;
        delete gaus2;
        delete legend;
    };

    

    // goodlooking
    h_vertex_data_uu->GetXaxis()->SetTitle("z_{0}-z_{1} [mm]");
    h_vertex_data_uu->GetYaxis()->SetTitle("Events / (40 mm)");

    h_vertex_data_cc->GetXaxis()->SetTitle("z_{0}-z_{1} [mm]");
    h_vertex_data_cc->GetYaxis()->SetTitle("Events / (40 mm)");

    //drawing
    c1->SaveAs((std::string("vertices_noATLAS_noTrig_filteredEvents_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());

    double_fitter(h_vertex_data_uu,names[0],names_who_are_you[3],-2000,2000,10,600,10,300);
    double_fitter(h_vertex_data_cc,names[1],names_who_are_you[3],-2000,2000,7,1000,7,200);

    c1->SaveAs((std::string("vertices_noATLAS_noTrig_filteredEvents_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    
