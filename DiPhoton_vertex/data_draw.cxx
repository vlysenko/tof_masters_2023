#include <iostream>
#include <iomanip>
#include <sstream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"


int main(int argc, char **argv)
{

    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }

    SetAtlasStyle();
    TCanvas *c1 = new TCanvas("c1", "c1", 0., 0., 800, 600);
    TFile inf(argv[1]);

    // setup

    auto n_bin = 50;

    auto nums = std::vector<int> {329716,329835,330025,330074,330079,330160,334264,334317,334890,334907,334993,335016,335083,335170,335282,335290,336567,336782,336832};
    auto colors = std::vector<int> {1,2,3,4,5,6,7,8,9,30,34,40,41,65,51,50,70,48,42};

    std::vector<TH1D *> hists(nums.size());

    for (size_t i = 0; i < nums.size(); i++)
        hists[i] = (TH1D *)inf.Get((std::string("ph01_vertex_DSs") + std::to_string(nums[i])).c_str());

    // start
    int runNumber = 429142;

    for (size_t i = 0; i < nums.size(); ++i)
    {
        hists[i]->GetXaxis()->SetTitle("z_{0}-z_{1} [mm]");
        hists[i]->GetYaxis()->SetTitle("Events");
    }
    for (size_t i = 0; i < 19; ++i)
        hists[i]->SetLineColor(colors[i]);

    TLegend *legend = new TLegend(0.18,0.35,0.3,0.92);
    legend->SetBorderSize(0);
    legend->SetTextSize(0.02);
    for (size_t i = 0; i < 19; ++i)
        legend->AddEntry(hists[i],("Data 2017, DS " + std::to_string(nums[i])).c_str(), "l");

    hists[18]->Draw();
    ATLASLabel(0.68, 0.87, "Internal");
    TLatex latex;
    latex.SetNDC();
    latex.SetTextSize(0.04);

    for (size_t i = 0; i < 18; ++i)
        hists[i]->Draw("same");

    legend->Draw();
    c1->SaveAs("1data_DSs.pdf");
    c1->Clear();
}