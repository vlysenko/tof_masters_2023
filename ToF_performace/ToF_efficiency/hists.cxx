#include <iostream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>
#include <TColor.h>
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        std::cout << "Requires 3 parameters\n";
        return 1;
    }

    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","Cc1",0.,0.,800,600);
    TFile *inf = new TFile(argv[1], "READ");
    TTree *tree_data = inf->Get<TTree>("CollectionTree");
    size_t nentries = tree_data->GetEntries();

    bool ATLASkey;
    std::string arg1(argv[2]);

    if (arg1=="atlas") {ATLASkey = true;}
    if (arg1=="noatlas") {ATLASkey = false;}

    // setup

    auto n_bin = 200;

    auto names = std::vector<std::string>{"0A", "0B", "0C", "0D", "1A", "1B", "1C", "1D", "2A", "2B", "2C", "2D", "3A", "3B", "3C", "3D"};
    auto channels = std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

    std::vector<TH1D *> hists_SiT_ToF_A_ON(names.size()), hists_SiT_ToF_C_ON(names.size()), hists_eff_A_ON(names.size()), hists_eff_C_ON(names.size()),
        hists_SiT_ToF_A_OFF(names.size()), hists_SiT_ToF_C_OFF(names.size()), hists_eff_A_OFF(names.size()), hists_eff_C_OFF(names.size());

    for (size_t i = 0; i < names.size(); i++)
    {
        hists_SiT_ToF_A_ON[i] = new TH1D((names[i] + std::string("_A_sitTof_on")).c_str(), (names[i] + std::string("_A_sitTof_on")).c_str(), n_bin, -20, 0);
        hists_SiT_ToF_C_ON[i] = new TH1D((names[i] + std::string("_C_sitTof_on")).c_str(), (names[i] + std::string("_C_sitTof_on")).c_str(), n_bin, -20, 0);
        hists_eff_A_ON[i] = new TH1D((names[i] + std::string("_A_eff_on")).c_str(), (names[i] + std::string("_A_eff_on")).c_str(), n_bin, -20, 0);
        hists_eff_C_ON[i] = new TH1D((names[i] + std::string("_C_eff_on")).c_str(), (names[i] + std::string("_C_eff_on")).c_str(), n_bin, -20, 0);

        hists_SiT_ToF_A_OFF[i] = new TH1D((names[i] + std::string("_A_sitTof_off")).c_str(), (names[i] + std::string("_A_sitTof_off")).c_str(), n_bin, -20, 0);
        hists_SiT_ToF_C_OFF[i] = new TH1D((names[i] + std::string("_C_sitTof_off")).c_str(), (names[i] + std::string("_C_sitTof_off")).c_str(), n_bin, -20, 0);
        hists_eff_A_OFF[i] = new TH1D((names[i] + std::string("_A_eff_off")).c_str(), (names[i] + std::string("_A_eff_off")).c_str(), n_bin, -20, 0);
        hists_eff_C_OFF[i] = new TH1D((names[i] + std::string("_C_eff_off")).c_str(), (names[i] + std::string("_C_eff_off")).c_str(), n_bin, -20, 0);
    }
    TH1D *hist_SiT_A = new TH1D("A_sit", "A_sit", n_bin, -20, 0);
    TH1D *hist_SiT_C = new TH1D("C_sit", "C_sit", n_bin, -20, 0);

    // start

    std::vector<float> *toFHit_channel = nullptr, *aFPTrack_xLocal = nullptr, *toFHit_trainID = nullptr,
                       *toFHit_stationID = nullptr, *aFPTrack_stationID = nullptr, *toFHit_time = nullptr;

    int runNumber;

    tree_data->SetBranchAddress("ToFHit_channel", &toFHit_channel);
    tree_data->SetBranchAddress("AFPTrack_xLocal", &aFPTrack_xLocal);
    tree_data->SetBranchAddress("ToFHit_trainID", &toFHit_trainID);
    tree_data->SetBranchAddress("ToFHit_stationID", &toFHit_stationID);
    tree_data->SetBranchAddress("AFPTrack_stationID", &aFPTrack_stationID);
    tree_data->SetBranchAddress("ToFHit_time", &toFHit_time);
    tree_data->SetBranchAddress("RunNumber", &runNumber);

    for (size_t i = 0; i < nentries; i++)
    //for (size_t i = 0; i < nentries/100; i++)
    {
        if (i % 500000 == 0)
            std::cout << i/100000 << " from " << nentries/100000 << "\n";
        tree_data->GetEntry(i);
        if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
            continue;

        // cut on only 1 train per event
        std::vector<int> vec_train_A(4, 0), vec_train_C(4, 0);
        for (size_t j = 0; j < toFHit_trainID->size(); j++)
        {
            for (size_t h = 0; h < 4; h++)
            {
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 0)
                    vec_train_A[h] = 1;
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 3)
                    vec_train_C[h] = 1;
            }
        }

        int sum_train_A = 0, sum_train_C = 0;
        for (size_t h = 0; h < 4; h++)
        {
            sum_train_A += vec_train_A[h];
            sum_train_C += vec_train_C[h];
        }

        // cut in only 1 track per event
        int sum_tracks_A = 0, sum_tracks_C = 0;
        for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
        {
            if (aFPTrack_stationID->at(j) == 0)
                sum_tracks_A++;
            if (aFPTrack_stationID->at(j) == 3)
                sum_tracks_C++;
        }

        // main loop for histograms with SiT&&ToF
        if (sum_tracks_A == 1 || sum_tracks_C == 1)
        {
            for (size_t j = 0; j < toFHit_channel->size(); ++j)
            {
                int chan = int(toFHit_channel->at(j));
                for (size_t k = 0; k < aFPTrack_xLocal->size(); ++k)
                {
                    if (aFPTrack_stationID->at(k) == toFHit_stationID->at(j) && chan < 16)
                    {
                        if (toFHit_stationID->at(j) == 0 && sum_tracks_A == 1)
                        {
                            hists_SiT_ToF_A_OFF[chan]->Fill(aFPTrack_xLocal->at(k));
                            if (sum_train_A == 1) {hists_SiT_ToF_A_ON[chan]->Fill(aFPTrack_xLocal->at(k));}
                        }
                        if (toFHit_stationID->at(j) == 3 && sum_tracks_C == 1)
                        {
                            hists_SiT_ToF_C_OFF[chan]->Fill(aFPTrack_xLocal->at(k));
                            if (sum_train_C == 1) {hists_SiT_ToF_C_ON[chan]->Fill(aFPTrack_xLocal->at(k));}
                        }
                    }
                }
            }
        }

        // main loop for histograms with SiT only
        for (size_t j = 0; j < aFPTrack_xLocal->size(); ++j)
        {
            if (aFPTrack_stationID->at(j) == 0 && sum_tracks_A == 1)
                hist_SiT_A->Fill(aFPTrack_xLocal->at(j));
            if (aFPTrack_stationID->at(j) == 3 && sum_tracks_C == 1)
                hist_SiT_C->Fill(aFPTrack_xLocal->at(j));
        }
    }
    // deviding hists to obtain efficiency
    for (size_t i = 0; i < 16; ++i)
    {
        hists_eff_A_OFF[i]->Divide(hists_SiT_ToF_A_OFF[i], hist_SiT_A, 1., 1., "B");
        hists_eff_C_OFF[i]->Divide(hists_SiT_ToF_C_OFF[i], hist_SiT_C, 1., 1., "B");
        hists_eff_A_ON[i]->Divide(hists_SiT_ToF_A_ON[i], hist_SiT_A, 1., 1., "B");
        hists_eff_C_ON[i]->Divide(hists_SiT_ToF_C_ON[i], hist_SiT_C, 1., 1., "B");
    }

    // calculation of efficiency
    auto effs_A_OFF = std::vector<float>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    auto effs_C_OFF = std::vector<float>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    auto effs_A_ON = std::vector<float>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    auto effs_C_ON = std::vector<float>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    for (size_t i = 0; i < 16; ++i)
    {
        if (i >= 0 && i < 4)
        {
            effs_A_OFF[i] = (hists_eff_A_OFF[i]->Integral(151, 180)) / (180 - 151);
            effs_C_OFF[i] = (hists_eff_C_OFF[i]->Integral(147, 180)) / (180 - 147);
            effs_A_ON[i] = (hists_eff_A_ON[i]->Integral(151, 180)) / (180 - 151);
            effs_C_ON[i] = (hists_eff_C_ON[i]->Integral(147, 180)) / (180 - 147);
        }
        if (i >= 4 && i < 8)
        {
            effs_A_OFF[i] = (hists_eff_A_OFF[i]->Integral(120, 150)) / (150 - 120);
            effs_C_OFF[i] = (hists_eff_C_OFF[i]->Integral(116, 146)) / (146 - 116);
            effs_A_ON[i] = (hists_eff_A_ON[i]->Integral(120, 150)) / (150 - 120);
            effs_C_ON[i] = (hists_eff_C_ON[i]->Integral(116, 146)) / (146 - 116);
        }
        if (i >= 8 && i < 12)
        {
            effs_A_OFF[i] = (hists_eff_A_OFF[i]->Integral(70, 119)) / (119 - 70);
            effs_C_OFF[i] = (hists_eff_C_OFF[i]->Integral(66, 115)) / (115 - 66);
            effs_A_ON[i] = (hists_eff_A_ON[i]->Integral(70, 119)) / (119 - 70);
            effs_C_ON[i] = (hists_eff_C_ON[i]->Integral(66, 115)) / (115 - 66);
        }
        if (i >= 12 && i < 16)
        {
            effs_A_OFF[i] = (hists_eff_A_OFF[i]->Integral(50, 69)) / (69 - 50);
            effs_C_OFF[i] = (hists_eff_C_OFF[i]->Integral(50, 65)) / (65 - 50);
            effs_A_ON[i] = (hists_eff_A_ON[i]->Integral(50, 69)) / (69 - 50);
            effs_C_ON[i] = (hists_eff_C_ON[i]->Integral(50, 65)) / (65 - 50);
        }
    }

    // output numbers
    std::cout << "1 Train cut OFF" << std::endl;
    std::cout << "side-A: ";
    for (size_t i = 0; i < 16; ++i)
        std::cout << effs_A_OFF[i] << ", ";
    std::cout << std::endl;
    std::cout << "side-C: ";
    for (size_t i = 0; i < 16; ++i)
        std::cout << effs_C_OFF[i] << ", ";
    std::cout << std::endl;

    std::cout << "1 Train cut ON" << std::endl;
    std::cout << "side-A: ";
    for (size_t i = 0; i < 16; ++i)
        std::cout << effs_A_ON[i] << ", ";
    std::cout << std::endl;
    std::cout << "side-C: ";
    for (size_t i = 0; i < 16; ++i)
        std::cout << effs_C_ON[i] << ", ";
    std::cout << std::endl;

    // goodlooking
    for (size_t i = 0; i < 16; ++i)
    {
        hists_eff_A_OFF[i]->GetXaxis()->SetTitle("x-position SiT [mm]");
        hists_eff_A_OFF[i]->GetYaxis()->SetTitle("Efficiency");
        hists_eff_A_OFF[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", OFF, Far-A, Channel ") + names[i]).c_str());
        hists_eff_A_OFF[i]->SetMarkerSize(0.8);

        hists_eff_C_OFF[i]->GetXaxis()->SetTitle("x-position SiT [mm]");
        hists_eff_C_OFF[i]->GetYaxis()->SetTitle("Efficiency");
        hists_eff_C_OFF[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", OFF, Far-C, Channel ") + names[i]).c_str());
        hists_eff_C_OFF[i]->SetMarkerSize(0.8);

        hists_eff_A_ON[i]->GetXaxis()->SetTitle("x-position SiT [mm]");
        hists_eff_A_ON[i]->GetYaxis()->SetTitle("Efficiency");
        hists_eff_A_ON[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", ON, Far-A, Channel ") + names[i]).c_str());
        hists_eff_A_ON[i]->SetMarkerSize(0.8);

        hists_eff_C_ON[i]->GetXaxis()->SetTitle("x-position SiT [mm]");
        hists_eff_C_ON[i]->GetYaxis()->SetTitle("Efficiency");
        hists_eff_C_ON[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", ON, Far-C, Channel ") + names[i]).c_str());
        hists_eff_C_ON[i]->SetMarkerSize(0.8);
    }

    // drawBox with effs
    int Nbin_drawBox = 4;
    TCanvas *c2 = new TCanvas("c2", "c2", 600, 600);
    auto trains = std::vector<std::string>{"0", "1", "2", "3"};
    auto bars = std::vector<std::string>{"A", "B", "C", "D"};
    auto names_for_drawBox = std::vector<std::string>{"hists_1tr_A", "hists_1tr_C", "hists_lot_tr_A", "hists_lot_tr_C"};
    auto good_names_for_drawBox = std::vector<std::string>{"Histos, 1 train ON, side-A", "Histos, 1 train ON, side-C", "Histos, 1 train OFF, side-A", "Histos, 1 train OFF, side-C"};

    std::vector<TH2D *> hists2d_for_drawBox(16);

    for (size_t i = 0; i < names_for_drawBox.size(); i++)
        hists2d_for_drawBox[i] = new TH2D((names_for_drawBox[i] + std::string("_h2d")).c_str(), (names_for_drawBox[i] + std::string("_eff_2d")).c_str(), Nbin_drawBox, 0, 4, Nbin_drawBox, 0, 4);

    auto effs_for_drawBox = std::vector<std::vector<float>>{effs_A_ON,effs_C_ON,effs_A_OFF,effs_C_OFF};

    for (size_t general = 0; general < names_for_drawBox.size(); general++)
        for (size_t i = 1; i < 5; i++)
            for (size_t j = 1; j < 5; j++)
            {
                float a=100*100*effs_for_drawBox[general][(4*(i-1))+(j-1)];
                a=round(a);
                a=a/100;
                hists2d_for_drawBox[general]->SetBinContent(j, i, a);
            }

    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        for (size_t i = 1; i < 5; i++)
        {
            hists2d_for_drawBox[general]->GetXaxis()->SetBinLabel(i, (bars[i-1]).c_str());
            hists2d_for_drawBox[general]->GetYaxis()->SetBinLabel(i, (trains[i-1]).c_str());
            hists2d_for_drawBox[general]->GetXaxis()->SetLabelSize(0.1);
            hists2d_for_drawBox[general]->GetYaxis()->SetLabelSize(0.1);
            hists2d_for_drawBox[general]->GetXaxis()->SetTitleSize(0.05);
            hists2d_for_drawBox[general]->GetYaxis()->SetTitleSize(0.05);
            hists2d_for_drawBox[general]->GetZaxis()->SetRangeUser(0,100);
            hists2d_for_drawBox[general]->SetMarkerSize(2);
        }
        hists2d_for_drawBox[general]->GetYaxis()->SetTitle("Trains");
        hists2d_for_drawBox[general]->GetXaxis()->SetTitle("Bars");
        hists2d_for_drawBox[general]->SetTitle((std::string("%, ") + std::to_string(runNumber) + std::string(" Run, ") + good_names_for_drawBox[general]).c_str());
    }
    
    /*
    //write in root file
    TFile *outf = new TFile("out.root", "RECREATE");
    for (size_t i = 0; i < names.size(); ++i)
    {
        hists_eff_A_OFF[i]->SetStats(0);
        hists_eff_A_OFF[i]->Draw();
        hists_eff_A_OFF[i]->Write();

        hists_eff_C_OFF[i]->SetStats(0);
        hists_eff_C_OFF[i]->Draw();
        hists_eff_C_OFF[i]->Write();

        hists_eff_A_ON[i]->SetStats(0);
        hists_eff_A_ON[i]->Draw();
        hists_eff_A_ON[i]->Write();

        hists_eff_C_ON[i]->SetStats(0);
        hists_eff_C_ON[i]->Draw();
        hists_eff_C_ON[i]->Write();
    }
    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        hists2d_for_drawBox[general]->SetStats(0);
        hists2d_for_drawBox[general]->Draw("text colz");
        hists2d_for_drawBox[general]->Write();
    }   
    outf->Close();
    */

    // write in pdf
    /*
    gStyle->SetPalette(55);
    c2->Clear();
    c2->SaveAs((std::string("DrawBox_hists_trig_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t general = 0; general < names_for_drawBox.size(); general++)
    {
        hists2d_for_drawBox[general]->SetStats(0);
        hists2d_for_drawBox[general]->Draw("text colz");
        c2->SaveAs((std::string("DrawBox_hists_trig_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c2->Clear();
    }   
    c2->SaveAs((std::string("DrawBox_hists_trig_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    */
    
    c1->Clear();
    c1->SaveAs((std::string("Effs_hists_oneTrainOFF_noATLAS_A_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < names.size(); ++i)
    {   
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        hists_eff_A_OFF[i]->Draw("eX0");
        if (i<8)
        {
            if (ATLASkey==1) {ATLASLabel(0.27,0.88,"Internal");}
            else {ATLASLabel_WithoutATLAS(0.27,0.94,"Internal");}
            latex.DrawLatex(0.27,0.83,(std::string("Run ") + std::to_string(runNumber) + std::string(", OFF, Far-A, ") + names[i]).c_str());
        }
        if ((i>7) && (i<12))
        {
            if (ATLASkey==1) {ATLASLabel(0.62,0.88,"Internal");}
            else {ATLASLabel_WithoutATLAS(0.59,0.94,"Internal");}
            latex.DrawLatex(0.59,0.83,(std::string("Run ") + std::to_string(runNumber) + std::string(", OFF, Far-A, ") + names[i]).c_str());
        }
        if (i>11)
        {
            if (ATLASkey==1) {ATLASLabel(0.6,0.88,"Internal");}
            else {ATLASLabel_WithoutATLAS(0.59,0.94,"Internal");}
            latex.DrawLatex(0.59,0.83,(std::string("Run ") + std::to_string(runNumber) + std::string(", OFF, Far-A, ") + names[i]).c_str());
        }
        c1->SaveAs((std::string("Effs_hists_oneTrainOFF_noATLAS_A_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    }
    c1->SaveAs((std::string("Effs_hists_oneTrainOFF_noATLAS_A_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();

    c1->SaveAs((std::string("Effs_hists_oneTrainOFF_noATLAS_C_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < names.size(); ++i)
    {
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        hists_eff_C_OFF[i]->Draw();
        if (i<8)
        {
            if (ATLASkey==1) {ATLASLabel(0.27,0.88,"Internal");}
            else {ATLASLabel_WithoutATLAS(0.27,0.94,"Internal");}
            latex.DrawLatex(0.27,0.83,(std::string("Run ") + std::to_string(runNumber) + std::string(", OFF, Far-C, ") + names[i]).c_str());
        }
        if ((i>7) && (i<12))
        {
            if (ATLASkey==1) {ATLASLabel(0.62,0.88,"Internal");}
            else {ATLASLabel_WithoutATLAS(0.62,0.94,"Internal");}
            latex.DrawLatex(0.62,0.83,(std::string("Run ") + std::to_string(runNumber) + std::string(", OFF, Far-C, ") + names[i]).c_str());
        }
        if (i>11)
        {
            if (ATLASkey==1) {ATLASLabel(0.6,0.88,"Internal");}
            else {ATLASLabel_WithoutATLAS(0.6,0.94,"Internal");}
            latex.DrawLatex(0.6,0.83,(std::string("Run ") + std::to_string(runNumber) + std::string(", OFF, Far-C, ") + names[i]).c_str());
        }
        c1->SaveAs((std::string("Effs_hists_oneTrainOFF_noATLAS_C_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    }
    c1->SaveAs((std::string("Effs_hists_oneTrainOFF_noATLAS_C_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();

    // same but for ON
    c1->SaveAs((std::string("Effs_hists_oneTrainON_noATLAS_A_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < names.size(); ++i)
    {
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        hists_eff_A_ON[i]->Draw();
        if (i<8)
        {
            if (ATLASkey==1) {ATLASLabel(0.2,0.88,"Internal");}
            else {ATLASLabel_WithoutATLAS(0.2,0.94,"Internal");}
            latex.DrawLatex(0.2,0.83,(std::string("Run ") + std::to_string(runNumber) + std::string(", ON, Far-A, ") + names[i]).c_str());
        }
        if ((i>7) && (i<12))
        {
            if (ATLASkey==1) {ATLASLabel(0.62,0.88,"Internal");}
            else {ATLASLabel_WithoutATLAS(0.6,0.80,"Internal");}
            latex.DrawLatex(0.6,0.69,(std::string("Run ") + std::to_string(runNumber) + std::string(", ON, Far-A, ") + names[i]).c_str());
        }
        if (i>11)
        {
            if (ATLASkey==1) {ATLASLabel(0.6,0.88,"Internal");}
            else {ATLASLabel_WithoutATLAS(0.6,0.80,"Internal");}
            latex.DrawLatex(0.6,0.69,(std::string("Run ") + std::to_string(runNumber) + std::string(", ON, Far-A, ") + names[i]).c_str());
        }
        c1->SaveAs((std::string("Effs_hists_oneTrainON_noATLAS_A_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    }
    c1->SaveAs((std::string("Effs_hists_oneTrainON_noATLAS_A_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();

    c1->SaveAs((std::string("Effs_hists_oneTrainON_noATLAS_C_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < names.size(); ++i)
    {
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        hists_eff_C_ON[i]->Draw();
        if (i<8)
        {
            if (ATLASkey==1) {ATLASLabel(0.2,0.88,"Internal");}
            else {ATLASLabel_WithoutATLAS(0.2,0.94,"Internal");}
            latex.DrawLatex(0.2,0.83,(std::string("Run ") + std::to_string(runNumber) + std::string(", ON, Far-C, ") + names[i]).c_str());
        }
        if ((i>7) && (i<12))
        {
            if (ATLASkey==1) {ATLASLabel(0.62,0.88,"Internal");}
            else {ATLASLabel_WithoutATLAS(0.62,0.94,"Internal");}
            latex.DrawLatex(0.62,0.83,(std::string("Run ") + std::to_string(runNumber) + std::string(", ON, Far-C, ") + names[i]).c_str());
        }
        if (i>11)
        {
            if (ATLASkey==1) {ATLASLabel(0.6,0.88,"Internal");}
            else {ATLASLabel_WithoutATLAS(0.6,0.94,"Internal");}
            latex.DrawLatex(0.6,0.83,(std::string("Run ") + std::to_string(runNumber) + std::string(", ON, Far-C, ") + names[i]).c_str());
        }
        c1->SaveAs((std::string("Effs_hists_oneTrainON_noATLAS_C_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    }
    c1->SaveAs((std::string("Effs_hists_oneTrainON_noATLAS_C_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();
    
    
}