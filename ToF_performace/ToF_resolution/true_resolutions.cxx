
#include "matrix.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

int main(int argc, char **argv)
{

    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }

    SetAtlasStyle();
    TCanvas *c1 = new TCanvas("c1", "c1", 0., 0., 800, 600);
    TFile inf(argv[1]);

    // setup

    //auto n_bin = 200;
    auto n_bin = 100;

    auto names = std::vector<std::string>{
        "0AB", "0AC", "0AD", "0BC", "0BD", "0CD",
        "1AB", "1AC", "1AD", "1BC", "1BD", "1CD",
        "2AB", "2AC", "2AD", "2BC", "2BD", "2CD",
        "3AB", "3AC", "3AD", "3BC", "3BD", "3CD"};

    std::vector<TH1D *> hists_A(names.size()), hists_C(names.size());

    for (size_t i = 0; i < names.size(); i++)
    {
        hists_A[i] = (TH1D *)inf.Get((names[i] + std::string("_A")).c_str());
        hists_C[i] = (TH1D *)inf.Get((names[i] + std::string("_C")).c_str());
    }

    // start
    int runNumber = 429142;

    std::vector<TH1D *> h_means_A(names.size()), h_means_C(names.size());
    for (size_t i = 0; i < 4; i++)
    { 
        h_means_A[i] = new TH1D((std::string("h_means_A_")+std::to_string(i)).c_str(), "", 6, 0, 6);
        h_means_C[i] = new TH1D((std::string("h_means_C_")+std::to_string(i)).c_str(), "", 6, 0, 6);
        h_means_A[i]->GetXaxis()->SetTitle("Channels combination");
        h_means_A[i]->GetYaxis()->SetTitle("FWHM [ps]");
        h_means_C[i]->GetXaxis()->SetTitle("Channels combination");
        h_means_C[i]->GetYaxis()->SetTitle("FWHM [ps]");
    }

    for (size_t i = 0; i < names.size(); ++i)
    {
        TF1* gaus_A = new TF1("gaus_A", "gaus");
        //gaus_A->SetLineColor(2);
        TFitResultPtr r_A = hists_A[i]->Fit("gaus_A","S");
        double sigma_A = r_A->Parameters()[2];
        double error_A = r_A->Errors()[2];

        TF1* gaus_C = new TF1("gaus_C", "gaus");
        //gaus_C->SetLineColor(2);
        TFitResultPtr r_C = hists_C[i]->Fit("gaus_C","S");
        double sigma_C = r_C->Parameters()[2];
        double error_C = r_C->Errors()[2];

        if (i<6)
        {
            h_means_A[0]->SetBinContent(i+1, sigma_A); 
            h_means_A[0]->SetBinError(i+1, error_A);
            h_means_C[0]->SetBinContent(i+1, sigma_C); 
            h_means_C[0]->SetBinError(i+1, error_C); 
        }
        if (i>5 && i<12)
        {
            h_means_A[1]->SetBinContent(i-5, sigma_A); 
            h_means_A[1]->SetBinError(i-5, error_A);
            h_means_C[1]->SetBinContent(i-5, sigma_C); 
            h_means_C[1]->SetBinError(i-5, error_C); 
        }
        if (i>11 && i<18)
        {
            h_means_A[2]->SetBinContent(i-11, sigma_A); 
            h_means_A[2]->SetBinError(i-11, error_A);
            h_means_C[2]->SetBinContent(i-11, sigma_C); 
            h_means_C[2]->SetBinError(i-11, error_C); 
        }
        if (i>17 && i<24)
        {
            h_means_A[3]->SetBinContent(i-17, sigma_A); 
            h_means_A[3]->SetBinError(i-17, error_A);
            h_means_C[3]->SetBinContent(i-17, sigma_C); 
            h_means_C[3]->SetBinError(i-17, error_C); 
        }
    }  

    auto drawATLASstuff = [&](float label_x, float label_y, std::string side, int train)
    {
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        ATLASLabel_WithoutATLAS(label_x,(label_y+0.11),"Internal");
        latex.DrawLatex(label_x,(label_y),(std::string("Run ") + std::to_string(runNumber)).c_str());
        latex.DrawLatex(label_x,(label_y-0.055),(std::string("Side ") + side + std::string(", ToF train ") + std::to_string(train)).c_str());
        c1->SaveAs((std::string("real_resolutions_single_") + side + std::string("_") + std::to_string(runNumber) + std::string("_big.pdf")).c_str());
        c1->Clear();
    };

    /*
    c1->SaveAs((std::string("real_resolutions_A_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    TF1 * tf_means_A = new TF1("tf_means_A","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    for (size_t i = 0; i < names.size(); ++i)
    {
        tf_means_A->SetParameter(0, 40); 
        tf_means_A->SetParameter(1, 40); 
        tf_means_A->SetParameter(2, 40); 
        tf_means_A->SetParameter(3, 40);
        tf_means_A->SetNpx(1000);
        h_means_A[i]->Fit(tf_means_A,"R");
        h_means_A[i]->Draw(); 
        tf_means_A->Draw("same");  
        c1->SaveAs((std::string("real_resolutions_A_") + std::to_string(runNumber) + std::string(".pdf")).c_str()); 
        c1->Clear();   
    }
    c1->SaveAs((std::string("real_resolutions_A_") + std::to_string(runNumber) + std::string(".pdf]")).c_str()); 
    c1->Clear();

    c1->SaveAs((std::string("real_resolutions_C_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    TF1 * tf_means_C = new TF1("tf_means_C","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    for (size_t i = 0; i < names.size(); ++i)
    {
        tf_means_C->SetParameter(0, 40); 
        tf_means_C->SetParameter(1, 40); 
        tf_means_C->SetParameter(2, 40); 
        tf_means_C->SetParameter(3, 40);
        tf_means_C->SetNpx(1000);
        h_means_C[i]->Fit(tf_means_C,"R");
        h_means_C[i]->Draw(); 
        tf_means_C->Draw("same");  
        c1->SaveAs((std::string("real_resolutions_C_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();    
    }
    c1->SaveAs((std::string("real_resolutions_C_") + std::to_string(runNumber) + std::string(".pdf]")).c_str()); 
    c1->Clear();
    */
    c1->SaveAs((std::string("real_resolutions_single_A_") + std::to_string(runNumber) + std::string("_big.pdf[")).c_str());
    TF1 * tf_means_A0 = new TF1("tf_means_A0","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A0->SetParameter(0, 40); 
    tf_means_A0->SetParameter(1, 40); 
    tf_means_A0->SetParameter(2, 40); 
    tf_means_A0->SetParameter(3, 40);
    tf_means_A0->SetNpx(1000);
    tf_means_A0->SetLineColor(2);
    //h_means_A[0]->SetStats(0);
    h_means_A[0]->Fit(tf_means_A0,"R");
    h_means_A[0]->Draw(); 
    tf_means_A0->Draw("same"); 
    drawATLASstuff(0.19,0.82,"A",0);

    TF1 * tf_means_A1 = new TF1("tf_means_A1","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A1->SetParameter(0, 40); 
    tf_means_A1->SetParameter(1, 40); 
    tf_means_A1->SetParameter(2, 40); 
    tf_means_A1->SetParameter(3, 40);
    tf_means_A1->SetNpx(1000);
    tf_means_A1->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_A[1]->Fit(tf_means_A1,"R");
    h_means_A[1]->Draw(); 
    tf_means_A1->Draw("same"); 
    drawATLASstuff(0.7,0.82,"A",1);

    TF1 * tf_means_A2 = new TF1("tf_means_A2","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A2->SetParameter(0, 40); 
    tf_means_A2->SetParameter(1, 40); 
    tf_means_A2->SetParameter(2, 40); 
    tf_means_A2->SetParameter(3, 40);
    tf_means_A2->SetNpx(1000);
    tf_means_A2->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_A[2]->Fit(tf_means_A2,"R");
    h_means_A[2]->Draw(); 
    tf_means_A2->Draw("same"); 
    drawATLASstuff(0.2,0.82,"A",2);

    TF1 * tf_means_A3 = new TF1("tf_means_A3","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A3->SetParameter(0, 40); 
    tf_means_A3->SetParameter(1, 40); 
    tf_means_A3->SetParameter(2, 40); 
    tf_means_A3->SetParameter(3, 40);
    tf_means_A3->SetNpx(1000);
    tf_means_A3->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_A[3]->Fit(tf_means_A3,"R");
    h_means_A[3]->Draw(); 
    tf_means_A3->Draw("same"); 
    drawATLASstuff(0.7,0.82,"A",3);

    c1->SaveAs((std::string("real_resolutions_single_A_") + std::to_string(runNumber) + std::string("_big.pdf]")).c_str());


    c1->SaveAs((std::string("real_resolutions_single_C_") + std::to_string(runNumber) + std::string("_big.pdf[")).c_str());
    TF1 * tf_means_C0 = new TF1("tf_means_C0","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C0->SetParameter(0, 40); 
    tf_means_C0->SetParameter(1, 40); 
    tf_means_C0->SetParameter(2, 40); 
    tf_means_C0->SetParameter(3, 40);
    tf_means_C0->SetNpx(1000);
    tf_means_C0->SetLineColor(2);
    //h_means_A[0]->SetStats(0);
    h_means_C[0]->Fit(tf_means_C0,"R");
    h_means_C[0]->Draw(); 
    tf_means_C0->Draw("same"); 
    drawATLASstuff(0.19,0.82,"C",0);
    
    TF1 * tf_means_C1 = new TF1("tf_means_C1","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C1->SetParameter(0, 40); 
    tf_means_C1->SetParameter(1, 40); 
    tf_means_C1->SetParameter(2, 40); 
    tf_means_C1->SetParameter(3, 40);
    tf_means_C1->SetNpx(1000);
    tf_means_C1->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_C[1]->Fit(tf_means_C1,"R");
    h_means_C[1]->Draw(); 
    tf_means_C1->Draw("same"); 
    drawATLASstuff(0.7,0.82,"C",1);

    TF1 * tf_means_C2 = new TF1("tf_means_C2","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C2->SetParameter(0, 40); 
    tf_means_C2->SetParameter(1, 40); 
    tf_means_C2->SetParameter(2, 40); 
    tf_means_C2->SetParameter(3, 40);
    tf_means_C2->SetNpx(1000);
    tf_means_C2->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_C[2]->Fit(tf_means_C2,"R");
    h_means_C[2]->Draw(); 
    tf_means_C2->Draw("same"); 
    drawATLASstuff(0.2,0.82,"C",2);

    TF1 * tf_means_C3 = new TF1("tf_means_C3","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C3->SetParameter(0, 40); 
    tf_means_C3->SetParameter(1, 40); 
    tf_means_C3->SetParameter(2, 40); 
    tf_means_C3->SetParameter(3, 40);
    tf_means_C3->SetNpx(1000);
    tf_means_C3->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_C[3]->Fit(tf_means_C3,"R");
    h_means_C[3]->Draw(); 
    tf_means_C3->Draw("same"); 
    drawATLASstuff(0.7,0.83,"C",3);

    c1->SaveAs((std::string("real_resolutions_single_C_") + std::to_string(runNumber) + std::string("_big.pdf]")).c_str());
}