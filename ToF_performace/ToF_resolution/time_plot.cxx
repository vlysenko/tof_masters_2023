#include <iostream>
#include <iomanip>
#include <sstream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"
#include <TRandom3.h>

int main(int argc, char **argv)
{
    SetAtlasStyle();

    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }

    TCanvas* c1 = new TCanvas("c1","c1",0.,0.,800,600);
    TFile *inf = new TFile(argv[1], "READ");
    TTree *tree_data = inf->Get<TTree>("CollectionTree");
    size_t nentries = tree_data->GetEntries();

    //setup
    auto n_bin = 200;
    //auto n_bin = 100;

    auto names = std::vector<std::string>{
        "0A", "0B", "0C", "0D",
        "1A", "1B", "1C", "1D",
        "2A", "2B", "2C", "2D",
        "3A", "3B", "3C", "3D"};

    auto channels = std::vector<int>{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};

    std::vector<TH1D *> hists_time(names.size()), hists_raw_time(names.size()), hists_raw_timeA(names.size()), hists_raw_timeC(names.size()), 
        hists_raw_timeA_close(names.size()), hists_raw_timeC_close(names.size());
    
    for (size_t i = 0; i < names.size(); i++)
    {
        hists_time[i] = new TH1D((names[i] + std::string("_time")).c_str(), (names[i] + std::string("_time")).c_str(), n_bin, 0, 30);
        hists_raw_time[i] = new TH1D((names[i] + std::string("_rawTime")).c_str(), (names[i] + std::string("_rawTime")).c_str(), 1024, 0, 1024);
        hists_raw_timeA[i] = new TH1D((names[i] + std::string("_A_rawTime")).c_str(), (names[i] + std::string("_A_rawTime")).c_str(), 1024, 0, 1024);
        hists_raw_timeC[i] = new TH1D((names[i] + std::string("_C_rawTime")).c_str(), (names[i] + std::string("_C_rawTime")).c_str(), 1024, 0, 1024);
        hists_raw_timeA_close[i] = new TH1D((names[i] + std::string("_A_rawTimeClose")).c_str(), (names[i] + std::string("_A_rawTimeClose")).c_str(), 150, 900, 1050);
        hists_raw_timeC_close[i] = new TH1D((names[i] + std::string("_C_rawTimeClose")).c_str(), (names[i] + std::string("_C_rawTimeClose")).c_str(), 200, 150, 350);
    }

    std::vector<float> *toFHit_channel = nullptr, *aFPTrack_xLocal = nullptr, *toFHit_trainID = nullptr,
                       *toFHit_stationID = nullptr, *aFPTrack_stationID = nullptr, *toFHit_time = nullptr;

    int runNumber;

    tree_data->SetBranchAddress("ToFHit_channel", &toFHit_channel);
    tree_data->SetBranchAddress("AFPTrack_xLocal", &aFPTrack_xLocal);
    tree_data->SetBranchAddress("ToFHit_trainID", &toFHit_trainID);
    tree_data->SetBranchAddress("ToFHit_stationID", &toFHit_stationID);
    tree_data->SetBranchAddress("AFPTrack_stationID", &aFPTrack_stationID);
    tree_data->SetBranchAddress("ToFHit_time", &toFHit_time);
    tree_data->SetBranchAddress("RunNumber", &runNumber);

    for (size_t i = 0; i < nentries; i++)
    //for (size_t i = 634594; i < 634595; i++)
    { 
        if (i % 9000000 == 0)
            std::cout << i/9000000 << " from " << nentries/9000000 << "\n";
        tree_data->GetEntry(i);
        if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
            continue;
        
        //cut on only 1 train per event
        std::vector<int> vec_train_A(4, 0), vec_train_C(4, 0);
        for (size_t j = 0; j < toFHit_trainID->size(); j++)
        {
            for (size_t h = 0; h < 4; h++)
            {
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 0)
                    vec_train_A[h] = 1;
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 3)
                    vec_train_C[h] = 1;
            }
        }

        int sum_train_A = 0, sum_train_C = 0;
        for (size_t h = 0; h < 4; h++)
        {
            sum_train_A += vec_train_A[h];
            sum_train_C += vec_train_C[h];
        }

        //cut in only 1 track per event
        int sum_tracks_A = 0, sum_tracks_C = 0;
        for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
        {
            if (aFPTrack_stationID->at(j) == 0)
                sum_tracks_A++;
            if (aFPTrack_stationID->at(j) == 3)
                sum_tracks_C++;
        }

        for (size_t j = 0; j < toFHit_channel->size(); j++)
        {
            int chan = (int)toFHit_channel->at(j);
            if (chan==16)
                continue;
            //hists_time[chan]->Fill(toFHit_time->at(j));
            float rawTime=(toFHit_time->at(j))/(25.0/1024.0);
            //if ((rawTime>200 && rawTime<300) || (rawTime>900 && rawTime<1000))
            {
                hists_raw_time[chan]->Fill(rawTime);
                hists_time[chan]->Fill(toFHit_time->at(j));
                //if ((toFHit_stationID->at(j))==0 && rawTime>900 && rawTime<1000 && sum_tracks_A==1)
                if ((toFHit_stationID->at(j))==0 && sum_tracks_A==1)
                {
                    hists_raw_timeA[chan]->Fill(rawTime);
                    hists_raw_timeA_close[chan]->Fill(rawTime);
                }
                //if ((toFHit_stationID->at(j))==3 && rawTime>200 && rawTime<300 && sum_tracks_C==1)
                if ((toFHit_stationID->at(j))==3 && sum_tracks_C==1)
                {
                    hists_raw_timeC[chan]->Fill(rawTime);
                    hists_raw_timeC_close[chan]->Fill(rawTime);
                }
            }

        }
    }

    for (size_t i = 0; i < names.size(); ++i)
    {
        hists_time[i]->GetXaxis()->SetTitle("time, ns");
        hists_time[i]->GetYaxis()->SetTitle("Events");
        hists_time[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", Raw Time, channel ") + names[i]).c_str());
        hists_time[i]->SetFillColor(kBlue-6);

        hists_raw_time[i]->GetXaxis()->SetTitle("HPTDC bins");
        hists_raw_time[i]->GetYaxis()->SetTitle("Events");
        hists_raw_time[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", Raw Time, channel ") + names[i]).c_str());
        hists_raw_time[i]->SetFillColor(kBlue-6);

        hists_raw_timeA[i]->GetXaxis()->SetTitle("HPTDC bins");
        hists_raw_timeA[i]->GetYaxis()->SetTitle("Events");
        hists_raw_timeA[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", Raw Time, Far-A, channel ") + names[i]).c_str());
        hists_raw_timeA[i]->SetFillColor(kBlue-6);

        hists_raw_timeC[i]->GetXaxis()->SetTitle("HPTDC bins");
        hists_raw_timeC[i]->GetYaxis()->SetTitle("Events");
        hists_raw_timeC[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", Raw Time, Far-C, channel ") + names[i]).c_str());
        hists_raw_timeC[i]->SetFillColor(kBlue-6);

        hists_raw_timeA_close[i]->GetXaxis()->SetTitle("HPTDC bins");
        hists_raw_timeA_close[i]->GetYaxis()->SetTitle("Events");
        hists_raw_timeA_close[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", Raw Time, Far-A, channel ") + names[i]).c_str());
        hists_raw_timeA_close[i]->SetFillColor(kBlue-6);

        hists_raw_timeC_close[i]->GetXaxis()->SetTitle("HPTDC bins");
        hists_raw_timeC_close[i]->GetYaxis()->SetTitle("Events");
        hists_raw_timeC_close[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", Raw Time, Far-C, channel ") + names[i]).c_str());
        hists_raw_timeC_close[i]->SetFillColor(kBlue-6);

    }

    //write in root file
    /*
    TFile *outf = new TFile("ToF_raw_time_stations_withTimeCut_429142.root", "RECREATE");
    for (size_t i = 0; i < names.size(); ++i)
    {
        hists_raw_time[i]->Draw();
        hists_raw_time[i]->Write();
    }
    outf->Close();
    */

    //write in pdf
    /*
    c1->SaveAs((std::string("ToF_time_withCut_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < names.size(); ++i)
    {
        TLegend* legend = new TLegend(0.46,0.7,0.73,0.65);
        legend->SetBorderSize(0);
        legend->AddEntry(hists_time[i],"Data 2022","pe");
        legend->SetTextSize(0.04);
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        hists_time[i]->Draw("bar");
        legend->Draw();
        ATLASLabel(0.46, 0.87,"Internal");
        latex.DrawLatex(0.46,0.73,(std::string("Run ") + std::to_string(runNumber) + std::string(", channel ") + names[i]).c_str());
        c1->SaveAs((std::string("ToF_time_withCut_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    }  
    c1->SaveAs((std::string("ToF_time_withCut_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();
    */
   
    c1->SaveAs((std::string("1ToF_raw_time_stations_withoutTimeCut_withoutATLAS_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < names.size(); ++i)
    {
        TLegend* legend = new TLegend(0.45,0.75,0.76,0.69);
        legend->SetBorderSize(0);
        legend->AddEntry(hists_raw_time[i],"Data 2022","f");
        legend->SetTextSize(0.04);
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        hists_raw_time[i]->Draw("bar");
        legend->Draw();
        //ATLASLabel(0.46, 0.87,"work in progress");
        ATLASLabel_WithoutATLAS(0.46, 0.87,"Work in progress");
        latex.DrawLatex(0.46,0.76,(std::string("Run ") + std::to_string(runNumber) + std::string(", channel ") + names[i]).c_str());
        c1->SaveAs((std::string("1ToF_raw_time_stations_withoutTimeCut_withoutATLAS_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    }  
    c1->SaveAs((std::string("1ToF_raw_time_stations_withoutTimeCut_withoutATLAS_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();
    
    /*
    c1->SaveAs((std::string("ToF_raw_time_stationA_withCut_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < names.size(); ++i)
    {
        hists_raw_timeA[i]->Draw("bar");
        c1->SaveAs((std::string("ToF_raw_time_stationA_withCut_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    }  
    c1->SaveAs((std::string("ToF_raw_time_stationA_withCut_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();

    c1->SaveAs((std::string("ToF_raw_time_stationC_withCut_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < names.size(); ++i)
    {
        hists_raw_timeA[i]->Draw("bar");
        c1->SaveAs((std::string("ToF_raw_time_stationC_withCut_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    }  
    c1->SaveAs((std::string("ToF_raw_time_stationC_withCut_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();

    c1->SaveAs((std::string("ToF_raw_time_stationA_withTimeCut_close_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < names.size(); ++i)
    {
        TLegend* legend = new TLegend(0.59,0.7,0.73,0.65);
        legend->SetBorderSize(0);
        legend->AddEntry(hists_raw_timeA_close[i],"Data 2022","pe");
        legend->SetTextSize(0.04);
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        hists_raw_timeA_close[i]->Draw("bar");
        legend->Draw();
        ATLASLabel(0.59, 0.87,"Internal");
        latex.DrawLatex(0.59,0.73,(std::string("Run ") + std::to_string(runNumber) + std::string(", channel ") + names[i]).c_str());
        c1->SaveAs((std::string("ToF_raw_time_stationA_withTimeCut_close_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    }  
    c1->SaveAs((std::string("ToF_raw_time_stationA_withTimeCut_close_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();

    c1->SaveAs((std::string("ToF_raw_time_stationC_withTimeCut_close_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < names.size(); ++i)
    {
        TLegend* legend = new TLegend(0.59,0.7,0.73,0.65);
        legend->SetBorderSize(0);
        legend->AddEntry(hists_raw_timeA_close[i],"Data 2022","pe");
        legend->SetTextSize(0.04);
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        hists_raw_timeA_close[i]->Draw("bar");
        legend->Draw();
        ATLASLabel(0.59, 0.87,"Internal");
        latex.DrawLatex(0.59,0.73,(std::string("Run ") + std::to_string(runNumber) + std::string(", channel ") + names[i]).c_str());
        c1->SaveAs((std::string("ToF_raw_time_stationC_withTimeCut_close_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    }  
    c1->SaveAs((std::string("ToF_raw_time_stationC_withTimeCut_close_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();
    */
}
