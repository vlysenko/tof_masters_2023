#include "matrix.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"
#include <TRandom3.h>

int main(int argc, char **argv)
{
    const auto nan = std::numeric_limits<double>::quiet_NaN();
    // Matrix<double> m{std::vector<double>{1.0,2.2,3.1,4.2,5.2,6.1}};
    
    if (argc != 3)
    {
        std::cout << "Requires 3 parameters\n";
        return 1;
    }

    SetAtlasStyle();
    TCanvas* c1 = new TCanvas("c1","c1",0.,0.,800,600);
    //TCanvas *c1=new TCanvas("c1", "c1", 600, 400);
    TFile *inf = new TFile(argv[1], "READ");
    TTree *tree_data = inf->Get<TTree>("CollectionTree");
    size_t nentries = tree_data->GetEntries();

    bool ATLASkey;
    std::string arg1(argv[2]);

    if (arg1=="atlas") {ATLASkey = true;}
    if (arg1=="noatlas") {ATLASkey = false;}

    //setup

    auto n_bin = 200;
    //auto n_bin = 100;

    auto names = std::vector<std::string>{
        "0AB", "0AC", "0AD", "0BC", "0BD", "0CD",
        "1AB", "1AC", "1AD", "1BC", "1BD", "1CD",
        "2AB", "2AC", "2AD", "2BC", "2BD", "2CD",
        "3AB", "3AC", "3AD", "3BC", "3BD", "3CD"};

    std::vector<TH1D *> hists_A(names.size()), hists_C(names.size());
    
    for (size_t i = 0; i < names.size(); i++)
    {
        hists_A[i] = new TH1D((names[i] + std::string("_A")).c_str(), (names[i] + std::string("_A")).c_str(), n_bin, -2000.0, 2000.0);
        hists_C[i] = new TH1D((names[i] + std::string("_C")).c_str(), (names[i] + std::string("_C")).c_str(), n_bin, -2000.0, 2000.0);
    }
    
    //hists_A[i] = new TH1D((names[i] + std::string("_A")).c_str(), (names[i] + std::string("_A")).c_str(), n_bin, -1000.0, 1000.0);
    //hists_C[i] = new TH1D((names[i] + std::string("_C")).c_str(), (names[i] + std::string("_C")).c_str(), n_bin, -1000.0, 1000.0);
    
    //start

    std::vector<float> *toFHit_channel = nullptr, *aFPTrack_xLocal = nullptr, *toFHit_trainID = nullptr,
                       *toFHit_stationID = nullptr, *aFPTrack_stationID = nullptr, *toFHit_time = nullptr;

    int runNumber;

    auto channel_diffs_reshaper = [&nan](const std::vector<float> *data_to_reshape, const std::vector<float> *channel_selection)
    {
        std::vector<float> outp(16, nan);
        for (size_t count = 0; count < channel_selection->size(); ++count)
            outp.at(channel_selection->at(count)) = data_to_reshape->at(count);
        Matrix<float> outp_matr{outp};
        outp_matr = outp_matr.reshape(4, 4);
        Matrix<float> diffs{4, 6, true};
        std::vector<float> diffs_iter{0, 1, 2, 3, 4, 5}, outp_iter_1{0, 0, 0, 1, 1, 2}, outp_iter_2{1, 2, 3, 2, 3, 3};
        for (size_t i = 0; i < diffs.rows(); ++i)
            for (size_t j = 0; j < diffs.cols(); ++j)
                diffs[i][diffs_iter[j]] = outp_matr[i][outp_iter_1[j]] - outp_matr[i][outp_iter_2[j]];
        return from_matrix_to_vector(diffs);
    };

    tree_data->SetBranchAddress("ToFHit_channel", &toFHit_channel);
    tree_data->SetBranchAddress("AFPTrack_xLocal", &aFPTrack_xLocal);
    tree_data->SetBranchAddress("ToFHit_trainID", &toFHit_trainID);
    tree_data->SetBranchAddress("ToFHit_stationID", &toFHit_stationID);
    tree_data->SetBranchAddress("AFPTrack_stationID", &aFPTrack_stationID);
    tree_data->SetBranchAddress("ToFHit_time", &toFHit_time);
    tree_data->SetBranchAddress("RunNumber", &runNumber);

    TRandom3 * random = new TRandom3();
    double smearTime = 0;
    for (size_t i = 0; i < nentries; i++)
    //for (size_t i = 0; i < nentries/100; i++)
    {
        if (i % 500000 == 0)
            std::cout << i/100000 << " from " << nentries/100000 << "\n";
        tree_data->GetEntry(i);
        if (toFHit_channel->size() == 0 || aFPTrack_xLocal->size() == 0)
            continue;

        //cut on only 1 train per event
        std::vector<int> vec_train_A(4, 0), vec_train_C(4, 0);
        for (size_t j = 0; j < toFHit_trainID->size(); j++)
        {
            for (size_t h = 0; h < 4; h++)
            {
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 0)
                    vec_train_A[h] = 1;
                if (toFHit_trainID->at(j) == h && toFHit_stationID->at(j) == 3)
                    vec_train_C[h] = 1;
            }
        }

        int sum_train_A = 0, sum_train_C = 0;
        for (size_t h = 0; h < 4; h++)
        {
            sum_train_A += vec_train_A[h];
            sum_train_C += vec_train_C[h];
        }

        //cut in only 1 track per event
        int sum_tracks_A = 0, sum_tracks_C = 0;
        for (size_t j = 0; j < aFPTrack_stationID->size(); j++)
        {
            if (aFPTrack_stationID->at(j) == 0)
                sum_tracks_A++;
            if (aFPTrack_stationID->at(j) == 3)
                sum_tracks_C++;
        }

        if (sum_tracks_A == 1 && sum_train_A == 1)
        {
            std::vector<float> toFHit_channel_stid_A, toFHit_time_stid_A;
            for (size_t count = 0; count < toFHit_channel->size(); ++count)
            {
                if (toFHit_stationID->at(count) == 0 && toFHit_channel->at(count)<16)
                {
                    toFHit_channel_stid_A.push_back(toFHit_channel->at(count));
                    smearTime = toFHit_time->at(count) + 0.025*(random->Uniform(1)-0.5);
                    //toFHit_time_stid_A.push_back(smearTime);
                    toFHit_time_stid_A.push_back(toFHit_time->at(count));
                }
            }
            for (auto &elem : toFHit_time_stid_A)
                if (1024 / 25 * elem > 1000 || 1024 / 25 * elem < 900)
                    elem = nan;
            auto diffs_A = channel_diffs_reshaper(&toFHit_time_stid_A, &toFHit_channel_stid_A);
            for (size_t count = 0; count < names.size(); ++count)
                if (!std::isnan(diffs_A[count]))
                    hists_A[count]->Fill(diffs_A[count]*(-1000));
        }

        if (sum_tracks_C == 1 && sum_train_C == 1)
        {
            std::vector<float> toFHit_channel_stid_C, toFHit_time_stid_C;
            for (size_t count = 0; count < toFHit_channel->size(); ++count)
            {
                if (toFHit_stationID->at(count) == 3 && toFHit_channel->at(count)<16)
                {
                    toFHit_channel_stid_C.push_back(toFHit_channel->at(count));
                    smearTime = toFHit_time->at(count) + 0.025*(random->Uniform(1)-0.5);
                    //toFHit_time_stid_C.push_back(smearTime);
                    toFHit_time_stid_C.push_back(toFHit_time->at(count));
                }
            }
            for (auto &elem : toFHit_time_stid_C)
                if (1024 / 25 * elem > 300 || 1024 / 25 * elem < 200)
                    elem = nan;
            auto diffs_C = channel_diffs_reshaper(&toFHit_time_stid_C, &toFHit_channel_stid_C);
            for (size_t count = 0; count < names.size(); ++count)
                if (!std::isnan(diffs_C[count]))
                    hists_C[count]->Fill(diffs_C[count]*(-1000));
        }

    }

    for (size_t i = 0; i < names.size(); ++i)
    {
        hists_A[i]->GetXaxis()->SetTitle("#Delta t [ps]");
        hists_A[i]->GetYaxis()->SetTitle("Events");
        hists_A[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", Far-A, Time difference, channels ") + names[i]).c_str());
        //hists_A[i]->SetFillColor(kBlue-6);
        //hists_A[i]->SetMarkerStyle(20);
        //hists_A[i]->SetMarkerSize(0.8);

        hists_C[i]->GetXaxis()->SetTitle("#Delta t [ps]");
        hists_C[i]->GetYaxis()->SetTitle("Events");
        hists_C[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", Far-C, Time difference, channels ") + names[i]).c_str());
        //hists_C[i]->SetFillColor(kBlue-6);
        //hists_C[i]->SetMarkerStyle(20);
        //hists_C[i]->SetMarkerSize(0.8);
    }

    auto ranges = [&](TH1D *hist, int userRangeDown, int userRangeUp)
    {
        hist->GetXaxis()->SetRangeUser(userRangeDown, userRangeUp);
    };


    ranges(hists_A[0],-1000,1000);
    ranges(hists_A[1],-1100,900);
    ranges(hists_A[2],-900,1100);
    ranges(hists_A[3],-1100,900);
    ranges(hists_A[4],-900,1100);
    ranges(hists_A[5],-800,1200);
    ranges(hists_A[6],-1200,800);
    ranges(hists_A[7],-1300,700);
    ranges(hists_A[8],-1400,600);
    ranges(hists_A[9],-1000,1000);
    ranges(hists_A[10],-1200,800);
    ranges(hists_A[11],-1100,900);
    ranges(hists_A[12],-1000,1000);
    ranges(hists_A[13],-800,1200);
    ranges(hists_A[14],-800,1200);
    ranges(hists_A[15],-1000,1000);
    ranges(hists_A[16],-1000,1000);
    ranges(hists_A[17],-1000,1000);
    ranges(hists_A[18],-1300,700);
    ranges(hists_A[19],-1200,800);
    ranges(hists_A[20],-1100,900);
    ranges(hists_A[21],-900,1100);
    ranges(hists_A[22],-800,1200);
    ranges(hists_A[23],-900,1100);

    ranges(hists_C[0],-1000,1000);
    ranges(hists_C[1],-1000,1000);
    ranges(hists_C[2],-800,1200);
    ranges(hists_C[3],-1000,1000);
    ranges(hists_C[4],-800,1200);
    ranges(hists_C[5],-800,1200);
    ranges(hists_C[6],-1300,700);
    ranges(hists_C[7],-1400,600);
    ranges(hists_C[8],-1300,700);
    ranges(hists_C[9],-1100,900);
    ranges(hists_C[10],-1000,1000);
    ranges(hists_C[11],-1000,1000);
    ranges(hists_C[12],-1000,1000);
    ranges(hists_C[13],-600,1400);
    ranges(hists_C[14],-600,1400);
    ranges(hists_C[15],-600,1400);
    ranges(hists_C[16],-600,1400);
    ranges(hists_C[17],-1000,1000);
    ranges(hists_C[18],-1300,700);
    ranges(hists_C[19],-1500,500);
    ranges(hists_C[20],-1100,900);
    ranges(hists_C[21],-1200,800);
    ranges(hists_C[22],-900,1100);
    ranges(hists_C[23],-700,1300);
    
    //start of fitting
    //write in root file
    
    TFile *outf = new TFile("resolutions200_435229.root", "RECREATE");
    for (size_t i = 0; i < names.size(); ++i)
    {
        //hists_A[i]->Fit("gaus");
        hists_A[i]->SetStats(0);
        //hists_A[i]->SetStats(1);
        //gStyle->SetOptFit();
        hists_A[i]->Draw();
        hists_A[i]->Write();

        //hists_C[i]->Fit("gaus");
        hists_C[i]->SetStats(0);
        //hists_C[i]->SetStats(1);
        //gStyle->SetOptFit();
        hists_C[i]->Draw();
        hists_C[i]->Write();
    }
    outf->Close();
    
    /*
    //write in pdf
    gStyle->SetOptFit();

    //c1->SaveAs((std::string("ToF_diff_200Bins_smearing_oncpp_A_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    c1->SaveAs((std::string("ToF_diff_200Bins_withoutATLAS_A_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < names.size(); ++i)
    {
        TF1* gaus1 = new TF1("gaus1", "gaus");
        gaus1->SetLineColor(2);
        
        TFitResultPtr r = hists_A[i]->Fit("gaus1","S");
        double sigma = r->Parameters()[2];
        double error = r->Errors()[2];
        std::stringstream stream_s;
        stream_s << std::fixed << std::setprecision(2) << sigma;
        std::string sigma_str = stream_s.str();
        std::stringstream stream_e;
        stream_e << std::fixed << std::setprecision(2) << error;
        std::string error_str = stream_e.str();
        
        TLegend* legend = new TLegend(0.2,0.72,0.5,0.85);
        legend->SetBorderSize(0);
        legend->AddEntry(hists_A[i],"Data 2022","pe");
        legend->AddEntry(gaus1,("#sigma = (" + sigma_str + " #pm " + error_str + ") ps").c_str(),"l");
        legend->SetTextSize(0.04);
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        hists_A[i]->Draw("eX0");
        legend->Draw();
        if (ATLASkey==1) {ATLASLabel(0.65,0.86,"Internal");}
        else {ATLASLabel_WithoutATLAS(0.65,0.86,"Internal");}
        latex.DrawLatex(0.208,0.86,(std::string("Run ") + std::to_string(runNumber) + std::string(", Far-A, ") + names[i]).c_str());
        //c1->SaveAs((std::string("ToF_diff_200Bins_smearing_oncpp_A_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->SaveAs((std::string("ToF_diff_200Bins_withoutATLAS_A_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    }  
    //c1->SaveAs((std::string("ToF_diff_200Bins_smearing_oncpp_A_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->SaveAs((std::string("ToF_diff_200Bins_withoutATLAS_A_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();
    
    //c1->SaveAs((std::string("ToF_diff_200Bins_smearing_oncpp_C_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    c1->SaveAs((std::string("ToF_diff_200Bins_withoutATLAS_C_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    for (size_t i = 0; i < names.size(); ++i)
    {
        TF1* gaus1 = new TF1("gaus1", "gaus");
        gaus1->SetLineColor(2);
        //TF1 *total = new TF1("total", "gaus(0)+gaus(3)");
        //total->SetLineColor(6);
        
        TFitResultPtr r = hists_C[i]->Fit("gaus1","S");
        double sigma = r->Parameters()[2];
        double error = r->Errors()[2];
        std::stringstream stream_s;
        stream_s << std::fixed << std::setprecision(2) << sigma;
        std::string sigma_str = stream_s.str();
        std::stringstream stream_e;
        stream_e << std::fixed << std::setprecision(2) << error;
        std::string error_str = stream_e.str();
        
        TLegend* legend = new TLegend(0.2,0.72,0.5,0.85);
        legend->SetBorderSize(0);
        legend->AddEntry(hists_C[i],"Data 2022","pe");
        legend->AddEntry(gaus1,("#sigma = (" + sigma_str + " #pm " + error_str + ") ps").c_str(),"l");
        legend->SetTextSize(0.04);
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        hists_C[i]->Draw("eX0");
        legend->Draw();
        if (ATLASkey==1) {ATLASLabel(0.65,0.86,"Internal");}
        else {ATLASLabel_WithoutATLAS(0.65,0.86,"Internal");}
        latex.DrawLatex(0.208,0.86,(std::string("Run ") + std::to_string(runNumber) + std::string(", Far-C, ") + names[i]).c_str());
        //c1->SaveAs((std::string("ToF_diff_200Bins_smearing_oncpp_C_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->SaveAs((std::string("ToF_diff_200Bins_withoutATLAS_C_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    }  
    //c1->SaveAs((std::string("ToF_diff_200Bins_smearing_oncpp_C_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->SaveAs((std::string("ToF_diff_200Bins_withoutATLAS_C_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
    c1->Clear();
    */
    //deleting
    /*
    for (size_t i = 0; i < names.size(); i++)
    {
        delete hists_A[i];
        delete hists_C[i];
    }
    */
    //delete[] hists_A;
    //delete[] hists_C;
}
