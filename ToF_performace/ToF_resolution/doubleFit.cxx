#include <iostream>
#include <iomanip>
#include <sstream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"


int main(int argc, char **argv)
{

    if (argc != 2)
    {
        std::cout << "Requires 2 parameters\n";
        return 1;
    }

    SetAtlasStyle();
    TCanvas *c1 = new TCanvas("c1", "c1", 0., 0., 800, 600);
    TFile inf(argv[1]);

    // setup

    auto n_bin = 200;
    //auto n_bin = 100;

    auto names = std::vector<std::string>{
        "0AB", "0AC", "0AD", "0BC", "0BD", "0CD",
        "1AB", "1AC", "1AD", "1BC", "1BD", "1CD",
        "2AB", "2AC", "2AD", "2BC", "2BD", "2CD",
        "3AB", "3AC", "3AD", "3BC", "3BD", "3CD"};

    std::vector<TH1D *> hists_A(names.size()), hists_C(names.size());

    for (size_t i = 0; i < names.size(); i++)
    {
        hists_A[i] = (TH1D *)inf.Get((names[i] + std::string("_A")).c_str());
        hists_C[i] = (TH1D *)inf.Get((names[i] + std::string("_C")).c_str());
    }

    // start
    //int runNumber = 429142;
    int runNumber = 435229;

    for (size_t i = 0; i < names.size(); ++i)
    {
        hists_A[i]->GetXaxis()->SetTitle("#Delta t [ps]");
        hists_A[i]->GetYaxis()->SetTitle("Events");
        //hists_A[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", Far-A, Time difference, channels ") + names[i]).c_str());
        // hists_A[i]->SetFillColor(kBlue-6);
        // hists_A[i]->SetMarkerStyle(20);
        // hists_A[i]->SetMarkerSize(0.8);

        hists_C[i]->GetXaxis()->SetTitle("#Delta t [ps]");
        hists_C[i]->GetYaxis()->SetTitle("Events");
        //hists_C[i]->SetTitle((std::string("Run ") + std::to_string(runNumber) + std::string(", Far-C, Time difference, channels ") + names[i]).c_str());
        // hists_C[i]->SetFillColor(kBlue-6);
        // hists_C[i]->SetMarkerStyle(20);
        // hists_C[i]->SetMarkerSize(0.8);
    }

    auto fitter = [&](TH1D *hist, std::string station, int index, int userRangeDown, int userRangeUp, int gausRangeDown, int gausRangeUp, 
    int gaus1mean, int gaus1width, int gaus2mean, int gaus2width)
    {
        hist->GetXaxis()->SetRangeUser(userRangeDown, userRangeUp);
        TF1 * totalFit = new TF1("totalFit","gaus(0)+gaus(3)", gausRangeDown,gausRangeUp); 
        totalFit->SetLineColor(2);
        TF1 * gaus1 = new TF1("gaus1","gaus(0)", gausRangeDown,gausRangeUp);
        gaus1->SetLineColor(4);
        gaus1->SetLineWidth(2);
        gaus1->SetLineStyle(4);
        TF1 * gaus2 = new TF1("gaus2","gaus(3)", gausRangeDown,gausRangeUp);
        gaus2->SetLineColor(6);
        gaus2->SetLineWidth(2);
        gaus2->SetLineStyle(2);

        totalFit->SetParameter(0, 10000); 
        totalFit->SetParameter(1, gaus1mean); 
        totalFit->SetParameter(2, gaus1width); 
        totalFit->SetParameter(3, 16000); 
        totalFit->SetParameter(4, gaus2mean); 
        totalFit->SetParameter(5, gaus2width);

        hist->Fit(totalFit,"R");
        Double_t par[6];
        Double_t er[6];
        totalFit->GetParameters(&par[0]);
        er[2]=totalFit->GetParError(2);
        er[5]=totalFit->GetParError(5);

        gaus1->SetParameter(0,(par[0]));
        gaus1->SetParameter(1,(par[1]));
        gaus1->SetParameter(2,(par[2]));
        gaus2->SetParameter(3,(par[3]));
        gaus2->SetParameter(4,(par[4]));
        gaus2->SetParameter(5,(par[5]));


        /*
        TF1 *g1 = new TF1("g1", "gaus", gaus1RangeDown, gaus1RangeUp);
        TF1 *g2 = new TF1("g2", "gaus", gaus2RangeDown, gaus2RangeUp);
        g2->SetParLimits(2, gaus2ParLimitSigmaDown, gaus2ParLimitSigmaUp);
        g2->SetParLimits(1, gaus2ParLimitMeanDown, gaus2ParLimitMeanUp);
        g1->SetLineColor(2);
        g2->SetLineColor(4);
        Double_t par[6];
        Double_t er[6];
        hist->Fit(g1, "R");
        hist->Fit(g2, "BR+");
        g1->GetParameters(&par[0]);
        g2->GetParameters(&par[3]);
        er[2] = g1->GetParError(2);
        er[5] = g2->GetParError(5);
        */
        std::stringstream stream_s1;
        stream_s1 << std::fixed << std::setprecision(2) << par[2];
        std::string sigma1_str = stream_s1.str();
        std::stringstream stream_e1;
        stream_e1 << std::fixed << std::setprecision(2) << er[2];
        std::string error1_str = stream_e1.str();
        std::stringstream stream_s2;
        stream_s2 << std::fixed << std::setprecision(2) << par[5];
        std::string sigma2_str = stream_s2.str();
        std::stringstream stream_e2;
        stream_e2 << std::fixed << std::setprecision(2) << er[5];
        std::string error2_str = stream_e2.str();
        TLegend *legend = new TLegend(0.2, 0.7, 0.45, 0.82);
        legend->SetBorderSize(0);
        legend->AddEntry(hist, "Data 2022", "pe");
        legend->AddEntry(gaus1, ("#sigma = (" + sigma1_str + " #pm " + error1_str + ") ps").c_str(), "l");
        legend->AddEntry(gaus2, ("#sigma = (" + sigma2_str + " #pm " + error2_str + ") ps").c_str(), "l");
        legend->SetTextSize(0.04);
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);

        hist->Draw("eX0");
        totalFit->Draw("same"); 
        gaus1->Draw("same"); 
        gaus2->Draw("same");
        legend->Draw();
        //ATLASLabel(0.65, 0.85, "Internal");
        ATLASLabel_WithoutATLAS(0.65,0.86,"Internal");
        latex.DrawLatex(0.2, 0.85, (std::string("Run ") + std::to_string(runNumber) + std::string(", Far-") + station + std::string(", ") + names[index]).c_str());
        c1->SaveAs((std::string("doubleSumGausAll_withoutATLAS_") + station + std::string("_") + std::to_string(runNumber) + std::string("_big.pdf")).c_str());
        c1->Clear();

        delete totalFit;
        delete gaus1;
        delete gaus2;
        delete legend;
    };

    // 2 gaus fit 
    c1->SaveAs((std::string("doubleSumGausAll_withoutATLAS_A_") + std::to_string(runNumber) + std::string("_big.pdf[")).c_str());

    fitter(hists_A[0],"A",0,-1000,1000,-400,400,0,40,30,30);
    fitter(hists_A[1],"A",1,-1100,900,-500,300,-100,20,0,20);
    fitter(hists_A[2],"A",2,-900,1100,-300,500,100,20,100,40);
    fitter(hists_A[3],"A",3,-1100,900,-500,200,-100,20,-100,40);
    fitter(hists_A[4],"A",4,-900,1100,-400,500,100,20,-100,10);
    fitter(hists_A[5],"A",5,-800,1200,-200,600,200,20,200,30);
    fitter(hists_A[6],"A",6,-1200,800,-500,200,-200,-200,0,30);
    fitter(hists_A[7],"A",7,-1300,700,-500,200,-300,20,0,30);
    fitter(hists_A[8],"A",8,-1400,600,-700,0,-400,20,-450,80);
    fitter(hists_A[9],"A",9,-1000,1000,-400,300,-50,20,-50,80);
    fitter(hists_A[10],"A",10,-1200,800,-500,100,-200,20,-400,20);
    fitter(hists_A[11],"A",11,-1100,900,-500,200,-150,20,-200,30);
    fitter(hists_A[12],"A",12,-1000,1000,-500,800,100,20,100,30);
    fitter(hists_A[13],"A",13,-800,1200,-200,600,200,20,200,20);
    fitter(hists_A[14],"A",14,-800,1200,-200,500,200,20,200,20);
    fitter(hists_A[15],"A",15,-1000,1000,-600,600,150,20,250,40);
    fitter(hists_A[16],"A",16,-1000,1000,-500,600,0,30,200,40);
    fitter(hists_A[17],"A",17,-1000,1000,-400,400,0,20,0,20);
    //fitter(hists_A[18],"A",18,-1300,700,-600,0,-300,20,-400,30);

    //for minBias
    fitter(hists_A[18],"A",18,-1300,700,-600,0,-300,40,-300,80);
    ////////

    fitter(hists_A[19],"A",19,-1200,800,-600,200,-300,20,0,20);
    //fitter(hists_A[20],"A",20,-1100,900,-500,300,-100,20,-100,30);

    //for minBias
    fitter(hists_A[20],"A",20,-1100,900,-500,300,-100,60,-250,30);
    /////////

    fitter(hists_A[21],"A",21,-900,1100,-300,500,100,20,200,30);
    //fitter(hists_A[22],"A",22,-800,1200,-200,600,300,20,100,20);

    //for minBias
    fitter(hists_A[22],"A",22,-800,1200,-200,600,300,20,200,60);
    /////////

    //fitter(hists_A[23],"A",23,-900,1100,-200,400,250,20,0,20);

    //for minBias
    fitter(hists_A[23],"A",23,-900,1100,-200,400,250,50,250,20);
    //////////

    c1->SaveAs((std::string("doubleSumGausAll_withoutATLAS_A_") + std::to_string(runNumber) + std::string("_big.pdf]")).c_str());
    c1->SaveAs((std::string("doubleSumGausAll_withoutATLAS_C_") + std::to_string(runNumber) + std::string("_big.pdf[")).c_str());

    fitter(hists_C[0],"C",0,-1000,1000,-200,300,0,20,0,30);
    fitter(hists_C[1],"C",1,-1000,1000,-300,400,50,30,50,80);
    fitter(hists_C[2],"C",2,-800,1200,-200,600,200,20,200,20);
    fitter(hists_C[3],"C",3,-1000,1000,-400,300,0,20,-100,30);
    fitter(hists_C[4],"C",4,-800,1200,-400,500,200,20,0,30);
    fitter(hists_C[5],"C",5,-800,1200,-200,500,200,20,200,30);
    fitter(hists_C[6],"C",6,-1300,700,-700,200,-300,20,-300,30);
    fitter(hists_C[7],"C",7,-1400,600,-800,0,-400,20,-500,20);
    fitter(hists_C[8],"C",8,-1300,700,-700,0,-300,20,-300,30);
    fitter(hists_C[9],"C",9,-1100,900,-400,200,-100,20,-300,20);
    fitter(hists_C[10],"C",10,-1000,1000,-500,300,-100,20,0,20);
    //fitter(hists_C[11],"C",11,-1000,1000,-200,400,100,10,300,30);
    
    //for minBias
    fitter(hists_C[11],"C",11,-1000,1000,-200,400,100,20,-50,40);
    ///////

    //fitter(hists_C[12],"C",12,-1000,1000,-800,800,0,20,-100,60);

    //for minBias
    fitter(hists_C[12],"C",12,-1000,1000,-800,800,450,80,-100,60);
    //////////

    fitter(hists_C[13],"C",13,-600,1400,0,800,450,20,300,80);
    fitter(hists_C[14],"C",14,-600,1400,0,700,400,20,200,20);
    fitter(hists_C[15],"C",15,-600,1400,-400,1200,500,20,200,20);
    fitter(hists_C[16],"C",16,-600,1400,-600,1200,500,20,200,20);
    //fitter(hists_C[17],"C",17,-1000,1000,-400,300,0,20,-300,20);

    //for minBias
    fitter(hists_C[17],"C",17,-1000,1000,-400,300,0,20,0,40);
    /////////

    fitter(hists_C[18],"C",18,-1300,700,-600,100,-300,20,-300,30);
    //fitter(hists_C[19],"C",19,-1500,500,-800,0,-500,20,-500,20);

    //for minBias
    fitter(hists_C[19],"C",19,-1500,500,-800,0,-500,20,-250,20);
    /////////

    //fitter(hists_C[20],"C",20,-1100,900,-400,200,-150,20,-200,30);

    //for minBias
    fitter(hists_C[20],"C",20,-1100,900,-400,200,-150,40,-150,30);
    ///////

    fitter(hists_C[21],"C",21,-1200,800,-500,200,-200,20,-200,30);
    fitter(hists_C[22],"C",22,-900,1100,-300,500,100,20,100,30);
    fitter(hists_C[23],"C",23,-700,1300,0,600,300,20,300,30);

    c1->SaveAs((std::string("doubleSumGausAll_withoutATLAS_C_") + std::to_string(runNumber) + std::string("_big.pdf]")).c_str());
}