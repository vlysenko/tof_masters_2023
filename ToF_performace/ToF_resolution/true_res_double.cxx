
#include "matrix.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <cmath>
#include <limits>
#include <TROOT.h>
#include <TStyle.h>
#include <TFitResultPtr.h>
#include <TFitResult.h>
#include <TF1.h>
#include <TLegend.h>
#include <TLatex.h>

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"

int main(int argc, char **argv)
{

    SetAtlasStyle();
    TCanvas *c1 = new TCanvas("c1", "c1", 0., 0., 800, 600);

    // setup

    auto n_bin = 200;
    //auto n_bin = 100;

    auto names = std::vector<std::string>{
        "0AB", "0AC", "0AD", "0BC", "0BD", "0CD",
        "1AB", "1AC", "1AD", "1BC", "1BD", "1CD",
        "2AB", "2AC", "2AD", "2BC", "2BD", "2CD",
        "3AB", "3AC", "3AD", "3BC", "3BD", "3CD"};


    /*
    //for 429142
    auto sigma_A = std::vector<double>{
        33.01, 42.49, 46.00, 30.51, 48.01, 47.60,
        42.42, 59.61, 69.13, 31.11, 45.95, 37.09,
        74.70, 57.40, 59.37, 124.92, 157.35, 62.80,
        69.36, 77.63, 83.68, 81.86, 80.28, 53.25};

    auto error_A = std::vector<double>{
        0.16,0.16,0.28,0.14,0.18,0.31,
        0.21,0.15,0.28,0.18,0.20,0.12,
        0.33,1.12,0.73,0.38,0.67,0.30,
        0.40,0.88,0.60,0.82,0.88,0.80};

    auto sigma_C = std::vector<double>{
        32.30, 40.51, 67.96, 30.44, 46.49, 79.14,
        63.94, 24.22, 67.03, 30.86, 37.26, 27.31,
        104.67, 47.56, 45.64, 61.15, 84.00, 50.11,
        50.61, 42.49, 45.28, 28.44, 30.11, 39.06};

    auto error_C = std::vector<double>{
        0.14,0.23,0.27,0.12,0.15,0.35,
        0.23,0.16,0.26,0.14,0.14,0.18,
        0.59,0.29,0.20,0.42,0.34,0.17,
        0.43,0.35,0.52,0.43,0.55,0.28};
    */
    
    
    //for 435229
    auto sigma_A = std::vector<double>{
        23.95,36.31,52.78,23.74,45.90,45.53,
        39.45,44.50,53.28,25.69,40.14,22.74,
        47.18,54.38,50.64,79.11,74.80,61.90,
        40.15,58.57,63.15,34.24,55.55,47.25};

    auto error_A = std::vector<double>{
        0.03,0.02,0.10,0.05,0.03,0.06,
        0.06,0.05,0.04,0.03,0.04,0.08,
        0.26,0.52,0.24,0.19,0.20,0.09,
        0.85,0.70,0.48,0.66,0.46,0.45};

    auto sigma_C = std::vector<double>{
        32.33,42.27,56.26,23.59,45.16,46.89,
        46.38,54.63,47.03,29.64,48.66,29.70,
        101.04,51.17,47.85,95.16,86.67,57.14,
        64.24,60.94,50.25,44.73,59.87,40.60};

    auto error_C = std::vector<double>{
        0.00,0.07,0.09,0.04,0.05,0.09,
        0.09,0.13,0.40,0.05,0.09,0.07,
        0.14,0.32,0.16,0.22,0.16,0.08,
        0.87,1.07,2.41,0.79,0.42,1.28};
    
    
    // start
    //int runNumber = 429142;
    int runNumber = 435229;

    std::vector<TH1D *> h_means_A(names.size()), h_means_C(names.size());
    for (size_t i = 0; i < 4; i++)
    { 
        h_means_A[i] = new TH1D((std::string("h_means_A_")+std::to_string(i)).c_str(), "", 6, 0, 6);
        h_means_C[i] = new TH1D((std::string("h_means_C_")+std::to_string(i)).c_str(), "", 6, 0, 6);
        h_means_A[i]->GetXaxis()->SetTitle("Channels combination");
        h_means_A[i]->GetYaxis()->SetTitle("FWHM [ps]");
        h_means_C[i]->GetXaxis()->SetTitle("Channels combination");
        h_means_C[i]->GetYaxis()->SetTitle("FWHM [ps]");
    }

    for (size_t i = 0; i < names.size(); ++i)
    {
        if (i<6)
        {
            h_means_A[0]->SetBinContent(i+1, sigma_A[i]); 
            h_means_A[0]->SetBinError(i+1, error_A[i]);
            h_means_C[0]->SetBinContent(i+1, sigma_C[i]); 
            h_means_C[0]->SetBinError(i+1, error_C[i]); 
        }
        if (i>5 && i<12)
        {
            h_means_A[1]->SetBinContent(i-5, sigma_A[i]); 
            h_means_A[1]->SetBinError(i-5, error_A[i]);
            h_means_C[1]->SetBinContent(i-5, sigma_C[i]); 
            h_means_C[1]->SetBinError(i-5, error_C[i]); 
        }
        if (i>11 && i<18)
        {
            h_means_A[2]->SetBinContent(i-11, sigma_A[i]); 
            h_means_A[2]->SetBinError(i-11, error_A[i]);
            h_means_C[2]->SetBinContent(i-11, sigma_C[i]); 
            h_means_C[2]->SetBinError(i-11, error_C[i]); 
        }
        if (i>17 && i<24)
        {
            h_means_A[3]->SetBinContent(i-17, sigma_A[i]); 
            h_means_A[3]->SetBinError(i-17, error_A[i]);
            h_means_C[3]->SetBinContent(i-17, sigma_C[i]); 
            h_means_C[3]->SetBinError(i-17, error_C[i]); 
        }
    }  

    /*
    c1->SaveAs((std::string("real_resolutions_A_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    TF1 * tf_means_A = new TF1("tf_means_A","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    for (size_t i = 0; i < names.size(); ++i)
    {
        tf_means_A->SetParameter(0, 40); 
        tf_means_A->SetParameter(1, 40); 
        tf_means_A->SetParameter(2, 40); 
        tf_means_A->SetParameter(3, 40);
        tf_means_A->SetNpx(1000);
        h_means_A[i]->Fit(tf_means_A,"R");
        h_means_A[i]->Draw(); 
        tf_means_A->Draw("same");  
        c1->SaveAs((std::string("real_resolutions_A_") + std::to_string(runNumber) + std::string(".pdf")).c_str()); 
        c1->Clear();   
    }
    c1->SaveAs((std::string("real_resolutions_A_") + std::to_string(runNumber) + std::string(".pdf]")).c_str()); 
    c1->Clear();

    c1->SaveAs((std::string("real_resolutions_C_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    TF1 * tf_means_C = new TF1("tf_means_C","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    for (size_t i = 0; i < names.size(); ++i)
    {
        tf_means_C->SetParameter(0, 40); 
        tf_means_C->SetParameter(1, 40); 
        tf_means_C->SetParameter(2, 40); 
        tf_means_C->SetParameter(3, 40);
        tf_means_C->SetNpx(1000);
        h_means_C[i]->Fit(tf_means_C,"R");
        h_means_C[i]->Draw(); 
        tf_means_C->Draw("same");  
        c1->SaveAs((std::string("real_resolutions_C_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();    
    }
    c1->SaveAs((std::string("real_resolutions_C_") + std::to_string(runNumber) + std::string(".pdf]")).c_str()); 
    c1->Clear();
    */

    auto drawATLASstuff = [&](float label_x, float label_y, std::string side, int train)
    {
        TLatex latex;
        latex.SetNDC();
        latex.SetTextSize(0.04);
        ATLASLabel_WithoutATLAS(label_x,(label_y+0.11),"Internal");
        latex.DrawLatex(label_x,(label_y),(std::string("Run ") + std::to_string(runNumber)).c_str());
        latex.DrawLatex(label_x,(label_y-0.055),(std::string("Side ") + side + std::string(", ToF train ") + std::to_string(train)).c_str());
        c1->SaveAs((std::string("real_resolutions_double_") + side + std::string("_") + std::to_string(runNumber) + std::string(".pdf")).c_str());
        c1->Clear();
    };


    c1->SaveAs((std::string("real_resolutions_double_A_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    TF1 * tf_means_A0 = new TF1("tf_means_A0","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A0->SetParameter(0, 40); 
    tf_means_A0->SetParameter(1, 40); 
    tf_means_A0->SetParameter(2, 40); 
    tf_means_A0->SetParameter(3, 40);
    tf_means_A0->SetNpx(1000);
    tf_means_A0->SetLineColor(2);
    //h_means_A[0]->SetStats(0);
    h_means_A[0]->Fit(tf_means_A0,"R");
    h_means_A[0]->Draw(); 
    tf_means_A0->Draw("same"); 
    drawATLASstuff(0.19,0.82,"A",0);
    
    TF1 * tf_means_A1 = new TF1("tf_means_A1","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A1->SetParameter(0, 40); 
    tf_means_A1->SetParameter(1, 40); 
    tf_means_A1->SetParameter(2, 40); 
    tf_means_A1->SetParameter(3, 40);
    tf_means_A1->SetNpx(1000);
    tf_means_A1->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_A[1]->Fit(tf_means_A1,"R");
    h_means_A[1]->Draw(); 
    tf_means_A1->Draw("same"); 
    drawATLASstuff(0.7,0.82,"A",1);

    TF1 * tf_means_A2 = new TF1("tf_means_A2","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A2->SetParameter(0, 40); 
    tf_means_A2->SetParameter(1, 40); 
    tf_means_A2->SetParameter(2, 40); 
    tf_means_A2->SetParameter(3, 40);
    tf_means_A2->SetNpx(1000);
    tf_means_A2->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_A[2]->Fit(tf_means_A2,"R");
    h_means_A[2]->Draw(); 
    tf_means_A2->Draw("same"); 
    drawATLASstuff(0.2,0.82,"A",2);

    TF1 * tf_means_A3 = new TF1("tf_means_A3","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_A3->SetParameter(0, 40); 
    tf_means_A3->SetParameter(1, 40); 
    tf_means_A3->SetParameter(2, 40); 
    tf_means_A3->SetParameter(3, 40);
    tf_means_A3->SetNpx(1000);
    tf_means_A3->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_A[3]->Fit(tf_means_A3,"R");
    h_means_A[3]->Draw(); 
    tf_means_A3->Draw("same"); 
    drawATLASstuff(0.7,0.82,"A",3);
    //drawATLASstuff(0.2,0.25,"A",3);

    c1->SaveAs((std::string("real_resolutions_double_A_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());

    c1->SaveAs((std::string("real_resolutions_double_C_") + std::to_string(runNumber) + std::string(".pdf[")).c_str());
    TF1 * tf_means_C0 = new TF1("tf_means_C0","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C0->SetParameter(0, 40); 
    tf_means_C0->SetParameter(1, 40); 
    tf_means_C0->SetParameter(2, 40); 
    tf_means_C0->SetParameter(3, 40);
    tf_means_C0->SetNpx(1000);
    tf_means_C0->SetLineColor(2);
    //h_means_A[0]->SetStats(0);
    h_means_C[0]->Fit(tf_means_C0,"R");
    h_means_C[0]->Draw(); 
    tf_means_C0->Draw("same"); 
    drawATLASstuff(0.19,0.82,"C",0);
    
    TF1 * tf_means_C1 = new TF1("tf_means_C1","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C1->SetParameter(0, 40); 
    tf_means_C1->SetParameter(1, 40); 
    tf_means_C1->SetParameter(2, 40); 
    tf_means_C1->SetParameter(3, 40);
    tf_means_C1->SetNpx(1000);
    tf_means_C1->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_C[1]->Fit(tf_means_C1,"R");
    h_means_C[1]->Draw(); 
    tf_means_C1->Draw("same"); 
    drawATLASstuff(0.7,0.82,"C",1);

    TF1 * tf_means_C2 = new TF1("tf_means_C2","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C2->SetParameter(0, 40); 
    tf_means_C2->SetParameter(1, 40); 
    tf_means_C2->SetParameter(2, 40); 
    tf_means_C2->SetParameter(3, 40);
    tf_means_C2->SetNpx(1000);
    tf_means_C2->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_C[2]->Fit(tf_means_C2,"R");
    h_means_C[2]->Draw(); 
    tf_means_C2->Draw("same"); 
    drawATLASstuff(0.7,0.84,"C",2);
    //drawATLASstuff(0.7,0.82,"C",2);

    TF1 * tf_means_C3 = new TF1("tf_means_C3","( (x>0) && (x<1) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaB]*[sigmaB]) + ( (x>1) && (x<2) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaC]*[sigmaC]) + ( (x>2) && (x<3) ) * TMath::Sqrt([sigmaA]*[sigmaA] + [sigmaD]*[sigmaD]) + ( (x>3) && (x<4) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaC]*[sigmaC]) + ( (x>4) && (x<5) ) * TMath::Sqrt([sigmaB]*[sigmaB] + [sigmaD]*[sigmaD]) + ( (x>5) && (x<6) ) * TMath::Sqrt([sigmaC]*[sigmaC] + [sigmaD]*[sigmaD])", 0.1,5.9); 
    tf_means_C3->SetParameter(0, 40); 
    tf_means_C3->SetParameter(1, 40); 
    tf_means_C3->SetParameter(2, 40); 
    tf_means_C3->SetParameter(3, 40);
    tf_means_C3->SetNpx(1000);
    tf_means_C3->SetLineColor(2);
    //h_means_A[1]->SetStats(0);
    h_means_C[3]->Fit(tf_means_C3,"R");
    h_means_C[3]->Draw(); 
    tf_means_C3->Draw("same"); 
    drawATLASstuff(0.7,0.82,"C",3);

    c1->SaveAs((std::string("real_resolutions_double_C_") + std::to_string(runNumber) + std::string(".pdf]")).c_str());
}